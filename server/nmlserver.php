<?php

// Server for Nutiteq NML data 
// For Nutiteq Maps SDK backend

// version: september 2014

// Usage: change data configurations in the beginning of file, and use url as nmlserver.php?data=yourdata

// Configuration
switch($_GET["data"]){
	case "my-pg-data":
		$connData = 'pgsql:host=localhost;port=5432;dbname=nml;user=postgres';
		break;

	case "my-nmldb-data":
		$connData = 'sqlite:/FULL_PATH_TO_MY_NMLDB_FILE';
		break;
}

// control caching with expires header
$expires= 60 * 60 * 24 * 14;

header('Pragma: public');
header('Cache-Control: max-age=' . $expires);
header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
header('Content-type: application/vnd.nutiteq.nml');

$dbh = new PDO($connData); 

switch($_GET["q"]){
	case "DataExtent":
		// world: http://localhost:8888/nmlapi/?q=DataExtent
		// response: gziplen, gzipped:
		//	mapbounds_x0(double8), mapbounds_x1(double8), mapbounds_y0(double8), mapbounds_y1(double8) - binary, no separator, bigendian

		$out = "";
		if (!isPostgresDb()) {
			$qry = "SELECT MIN(mapbounds_x0) AS mapbounds_x0, MAX(mapbounds_x1) AS mapbounds_x1, MIN(mapbounds_y0) AS mapbounds_y0, MAX(mapbounds_y1) AS mapbounds_y1 FROM MapTiles";
		} else {
			$qry = "SELECT MIN(ST_XMin(ST_Extent(mapbounds))) AS mapbounds_x0, MAX(ST_XMax(ST_Extent(mapbounds))) AS mapbounds_x1, MIN(ST_YMin(ST_Extent(mapbounds))) AS mapbounds_y0, MAX(ST_YMax(ST_Extent(mapbounds))) AS mapbounds_y1 FROM MapTiles";
		}
		$sth = $dbh->prepare($qry);
		if (!$sth) {
			error_log($dbh->errorInfo());
		} else {
			$sth->execute();
			$row = $sth->fetchObject();
			if (!is_null($row->mapbounds_x0)) {
				$out .= asDouble($row->mapbounds_x0);
				$out .= asDouble($row->mapbounds_x1);
				$out .= asDouble($row->mapbounds_y0);
				$out .= asDouble($row->mapbounds_y1);
			}
		}

		// Done
		$gzout = gzencode($out, -1, FORCE_GZIP);
		echo asInt(strlen($gzout));
		echo $gzout;
		break;

	case "MapTiles":
		// world: http://localhost:8888/nmlapi/?q=MapTiles&mapbounds_x0=-2000000&mapbounds_x1=20000000&mapbounds_y0=-20000000&mapbounds_y1=20000000&width=40000000
		// response: gziplen (int4), gzipped:
		//    array{id (int4), modellodtree_id (int4), mappos_x(double8), mappos_y(double8), groundheight(double8)}

		$envelopeX0 = $_GET["mapbounds_x0"];
		$envelopeX1 = $_GET["mapbounds_x1"];
		$envelopeY0 = $_GET["mapbounds_y0"];
		$envelopeY1 = $_GET["mapbounds_y1"];
		$width = $_GET["width"];

		// Map tiles corresponding to envelope
		$out = "";
		if (!isPostgresDb()) {
			$qry = "SELECT id, modellodtree_id, mappos_x, mappos_y, groundheight FROM MapTiles WHERE ((mapbounds_x1>=? AND mapbounds_x0<=?) OR (mapbounds_x1>=?+".$width." AND mapbounds_x0<=?+".$width.") OR (mapbounds_x1>=?-".$width." AND mapbounds_x0<=?-".$width. " AND (mapbounds_y1>=? AND mapbounds_y0<=?)))";
			$sth = $dbh->prepare($qry);
			if ($sth) {
				$sth->execute(array($envelopeX0, $envelopeX1, $envelopeX0, $envelopeX1, $envelopeX0, $envelopeX1, $envelopeY0, $envelopeY1));
			}
		} else {
			$qry = "SELECT id, modellodtree_id, ST_X(mappos) AS mappos_x, ST_Y(mappos) as mappos_y, groundheight FROM MapTiles WHERE ST_Intersects(mapbounds, ST_SetSRID(ST_MakeBox2D(ST_Point(?, ?), ST_Point(?, ?)), 3857))";
			$sth = $dbh->prepare($qry);
			if ($sth) {
				$sth->execute(array($envelopeX0, $envelopeY0, $envelopeX1, $envelopeY1));
			}
		}
		if (!$sth) {
			error_log($dbh->errorInfo());
		} else {
			while ($row = $sth->fetchObject()) {
				$out .= asLong($row->id);
				$out .= asLong($row->modellodtree_id);
				$out .= asDouble($row->mappos_x);
				$out .= asDouble($row->mappos_y);
				$out .= asDouble($row->groundheight);
			}
		}
		$out .= asLong(-1);

		// Done
		$gzout = gzencode($out, -1, FORCE_GZIP);
		echo asInt(strlen($gzout));
		echo $gzout;
		break;
	
	case "ModelLODTree":
		// test: http://localhost:8888/nmlapi/?q=ModelLODTree&id=1
		// response: gziplen, gzipped:
		// 	len (int4), nmlmodellodtree bin
		// 	array{model_id (int4), folder (asUtf8), mappos_x (double), mappos_y (double), groundheight (double)}
		// 	array{node_id (int4), local_id (asUtf8), mesh_id (int4)}
		// 	array{node_id (int4), local_id (asUtf8), texture_id (int4), level (int4)}
	
		$id = $_GET["id"];

		// NML lod tree
		$out = "";
		$qry = "SELECT id, nmlmodellodtree FROM ModelLODTrees WHERE id=?";
		$sth = $dbh->prepare($qry);
		if (!$sth) {
			error_log($dbh->errorInfo());
			$out .= asInt(0);
		} else {
			$sth->execute(array($id));
			$row = $sth->fetchObject();
			if (!$row) {
				$out .= asInt(0);
			} else {
				$nmlmodellodtree = readBlob($row->nmlmodellodtree);
				$out .= asInt(strlen($nmlmodellodtree));
				$out .= $nmlmodellodtree; // just binary
			}
		}

		// Model info
		$qry = "SELECT model_id, folder, mappos_x, mappos_y, groundheight FROM ModelInfo WHERE modellodtree_id=?";
		$sth = $dbh->prepare($qry);
		if (!$sth) {
			error_log($dbh->errorInfo());
		} else {
			$sth->execute(array($id));
			while ($row = $sth->fetchObject()) {
				$out .= asInt($row->model_id);
				$out .= asUtf8($row->folder);
				$out .= asDouble($row->mappos_x);
				$out .= asDouble($row->mappos_y);
				$out .= asDouble($row->groundheight);
			}
		}
		$out .= asInt(-1);

		// Mesh bindings
		$qry = "SELECT node_id, local_id, mesh_id FROM ModelLODTreeNodeMeshes WHERE modellodtree_id=?";
		$sth = $dbh->prepare($qry);
		if (!$sth) {
			error_log($dbh->errorInfo());
		} else {
			$sth->execute(array($id));
			while ($row = $sth->fetchObject()) {
				$out .= asInt($row->node_id);
				$out .= asUtf8($row->local_id);
				$out .= asLong($row->mesh_id);
			}
		}
		$out .= asInt(-1);

		// Texture bindings
		$qry = "SELECT node_id, local_id, texture_id, level FROM ModelLODTreeNodeTextures WHERE modellodtree_id=?";
		$sth = $dbh->prepare($qry);
		if (!$sth) {
			error_log($dbh->errorInfo());
		} else {
			$sth->execute(array($id));
			while ($row = $sth->fetchObject()) {
				$out .= asInt($row->node_id);
				$out .= asUtf8($row->local_id);
				$out .= asLong($row->texture_id);
				$out .= asInt($row->level);
			}
		}
		$out .= asInt(-1);

		// Done
		$gzout = gzencode($out, -1, FORCE_GZIP);
		echo asInt(strlen($gzout));
		echo $gzout;
		break;

	case "Meshes":
		// test: http://localhost:8888/nmlapi/?q=Meshes&ids=1,2
		// response: array{id (int4), gziplen (int4), nmlmesh (gzipped bin)}

		$ids = explode(",", $_GET["ids"]);
		$arg = join(",", array_fill(0, count($ids), "?"));
		$qry = "SELECT id, nmlmesh FROM Meshes WHERE id IN (" . $arg . ")";
		$sth = $dbh->prepare($qry);
		if (!$sth) {
			error_log($dbh->errorInfo());
		} else {
			$sth->execute($ids);
			while ($row = $sth->fetchObject()) {
				$nmlmesh = readBlob($row->nmlmesh);
				$gznmlmesh = gzencode($nmlmesh, -1, FORCE_GZIP);
				echo asLong($row->id);
				echo asInt(strlen($gznmlmesh));
				echo $gznmlmesh; // just binary
			}
		}
		echo asLong(-1);
		break;
	
	case "Textures":
		// test: http://localhost:8888/nmlapi/?q=Textures&ids=1,2
		// response: array{id (int4), gziplen (int4), nmltexture (gzipped bin)}

		$ids = explode(",", $_GET["ids"]);
		$arg = join(",", array_fill(0, count($ids), "?"));
		$qry = "SELECT id, nmltexture FROM Textures WHERE id IN (" . $arg . ") AND level=0";
		$sth = $dbh->prepare($qry);
		if (!$sth) {
			error_log($dbh->errorInfo());
		} else {
			$sth->execute($ids);
			while ($row = $sth->fetchObject()) {
				$nmltexture = readBlob($row->nmltexture);
				$gznmltexture = gzencode($nmltexture, -1, FORCE_GZIP);
				echo asLong($row->id);
				echo asInt(strlen($gznmltexture));
				echo $gznmltexture; // just binary
			}
		}
		echo asLong(-1);
		break;

} // switch(q)

function isPostgresDb() {
	return substr($GLOBALS["connData"], 0, 6) == 'pgsql:';
}

function readBlob($blob){
	// If not Postgres, simply return blob string
	if (!isPostgresDb()) {
		return $blob;
	}

	// Postgres gives handle for binary data, read it block-by-block and concatenate
	$data = '';
	do {
		$block = fread($blob, 65536);
		$data .= $block;
	} while (strlen($block) == 65536);
	return $data;
}

function asLong($long){
	// 2 longs (N) in php is long in Java
	// NOTE: this really works only on 64-bit platforms, all integers will be truncated to 32-bit otherwise!
	$intHigh = ($long & 0xffffffff00000000) >> 32;
	$intLow  = ($long & 0x00000000ffffffff) >> 0;
	if ($long == -1) {
		$intHigh = -1; $intLow = -1;
	}
	return pack("NN",$intHigh,$intLow);
}

function asInt($int){
	// long (N) in php is int in Java
	return pack("N",$int);
}

function asUtf8($txt){
	// Java representation of utf8
	return pack("n",strlen(utf8_encode($txt))) . utf8_encode($txt);
}

function asDouble($f){
	// reverse to make it bigendian?
	if (unpack("L",pack("N",1)) != 1) {
		return strrev(pack("d",$f));
	}
	return pack("d",$f);
}

?>

