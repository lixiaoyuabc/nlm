/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_TEXTURE_H__
#define __NMLFRAMEWORK_TEXTURE_H__

#include "Prerequisites.h"
#include "BaseTypes.h"
#include "Image.h"

#include <boost/shared_ptr.hpp>
#include <boost/functional/hash.hpp>


namespace NMLFramework
{
	class TextureExporter;

	class Texture
	{

	public:

		typedef NMLPackage::Sampler Sampler;

		/** Default constructor */
		Texture() : mImage(new Image()) {}

		/** Access to texture attributes */
		Image &getImage() { return *mImage; }
		const Image &getImage() const { return *mImage; }
		void setImage(const Image &image) { mImage.reset(new Image(image)); }
		Sampler &getSampler() { return mSampler; }
		const Sampler &getSampler() const { return mSampler; }
		void setSampler(const Sampler &sampler) { mSampler = sampler; }

		/** Clone texture */
		Texture clone() const;

		/** Calculate average texture color */
		ColorRGBA calculateAvgColor() const;

		/** Estimate texture footprint in memory */
		int estimateMemoryFootprint(const TextureExporter &textureExporter) const;

		/** Resize texture */
		void resize(float scale);

		/** Resize texture to power-of-two size, given maximum size */
		void resizeToPOT(float scale, int maxSize);

		/** Export/import texture as NMLTexture */
		bool saveNMLTexture(const std::string &id, NMLPackage::Texture &nmlTexture, const TextureExporter &textureExporter) const;
		bool loadNMLTexture(const NMLPackage::Texture &nmlTexture);

		/** Operators */
		size_t hash() const { size_t seed = 0; boost::hash_combine(seed, mSampler.SerializeAsString()); boost::hash_combine(seed, *mImage); return seed; }
		bool operator == (const Texture &texture) const { return *mImage == *texture.mImage && mSampler.SerializeAsString() == texture.mSampler.SerializeAsString(); }
		bool operator != (const Texture &texture) const { return !(*this == texture); }

		/** Calculate smallest power-of-two value not less than given value */
		static int roundToPOT(int value);

	private:

		boost::shared_ptr<Image> mImage;
		Sampler mSampler;
	};

	inline size_t hash_value(const Texture &texture)
	{
		return texture.hash();
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_TEXTURE_H__
