/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_DAEPREPROCESSOR_H__
#define __NMLFRAMEWORK_DAEPREPROCESSOR_H__

#include "Prerequisites.h"
#include "Processor.h"

#include "COLLADABUURI.h"

#include "URILoader.h"


namespace NMLFramework
{

	class DAEPreprocessor : public Processor
	{

	public:

		DAEPreprocessor(const COLLADABU::URI& inputUri, URILoader& loader, const std::string& outputFile);
		virtual ~DAEPreprocessor();

		/** Preprocess DAE, save new DAE file */
		bool preprocess();

	protected:

		URILoader &mURILoader;
		COLLADABU::URI mInputUri;
		std::string mOutputFile;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_DAEPREPROCESSOR_H__
