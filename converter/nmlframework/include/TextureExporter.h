/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_TEXTUREEXPORTER_H__
#define __NMLFRAMEWORK_TEXTUREEXPORTER_H__

#include "Prerequisites.h"
#include "Processor.h"
#include "Texture.h"

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>


namespace NMLFramework
{

	class TextureExporter : public Processor
	{

	public:

		class ExportException : public std::runtime_error
		{
		public:
			ExportException(const std::string &msg) : std::runtime_error(msg) { }
		};

		struct Options
		{
			enum Format { RAW, ETC1, PVRTC1_2BPP, PVRTC1_4BPP, DXTC1, DXTC3, DXTC5, PNG, JPEG };

			bool generateMipMaps;
			Format RGBFormat;
			Format RGBAFormat;

			Options() : generateMipMaps(false), RGBFormat(ETC1), RGBAFormat(ETC1) { }

			void setVariables(const boost::program_options::variables_map &vm)
			{
				generateMipMaps = vm["generate-mipmaps"].as<bool>();
				RGBFormat = vm["compress-rgb"].as<Format>();
				RGBAFormat = vm["compress-rgba"].as<Format>();
			}

			static boost::program_options::options_description getDescription()
			{
				namespace po = boost::program_options;
				static const Options opt;
				po::options_description desc("Texture options");
				desc.add_options()
					("generate-mipmaps", po::value<bool>()->default_value(opt.generateMipMaps), "generate texture mipmaps")
					("compress-rgb", po::value<Format>()->default_value(opt.RGBFormat), "compress RGB textures using method (raw, etc1, pvrtc1_2bpp, pvrtc1_4bpp, dxtc1, dxtc3, dxtc5)")
					("compress-rgba", po::value<Format>()->default_value(opt.RGBAFormat), "compress RGBA textures using method (raw, etc1, pvrtc1_2bpp, pvrtc1_4bpp, dxtc1, dxtc3, dxtc5)")
				;
				return desc;
			}
		};

		TextureExporter(const Options &options) : mOptions(options) { }

		/** Estimate texture footprint in memory */
		int estimateMemoryFootprint(const Texture &texture) const;

		/** Export/import texture */
		bool exportNMLTexture(const Texture &texture, const std::string &id, NMLPackage::Texture &nmlTexture) const;
		static bool importNMLTexture(const NMLPackage::Texture &nmlTexture, Texture &texture);

		/** Options */
		static Options &getDefaultOptions() { return sDefaultOptions; }

	private:

		enum { DEFAULT_JPEG_QUALITY = 100, DEFAULT_PNG_COMPRESSION = 1 };

		const Options mOptions;
		static Options sDefaultOptions;
	};

	std::istream &operator >> (std::istream &is, TextureExporter::Options::Format &format);
	std::ostream &operator << (std::ostream &os, TextureExporter::Options::Format format);

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_TEXTUREEXPORTER_H__
