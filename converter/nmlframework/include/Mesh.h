/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_MESH_H__
#define __NMLFRAMEWORK_MESH_H__

#include "Prerequisites.h"
#include "BaseTypes.h"
#include "Submesh.h"

#include <list>

#include <boost/functional/hash.hpp>


namespace NMLFramework
{

	class Mesh
	{

	public:

		typedef std::list<Submesh> SubmeshList;

		/** Default constructor */
		Mesh() { }

		/** Access to mesh attributes */
		SubmeshList &getSubmeshList() { return mSubmeshList; }
		const SubmeshList &getSubmeshList() const { return mSubmeshList; }
		int getVertexCount() const;

		/** Set all vertex ids of this mesh */
		void setVertexIds(int id);

		/** Calculate bounds with given transform */
		Bounds3 calculateBounds(const Matrix4 &transform) const;

		/** Clone mesh */
		Mesh clone() const;

		/** Apply transformation to mesh */
		void applyTransformation(const Matrix4 &transform);

		/** Export/import mesh as NMLMesh */
		bool saveNMLMesh(const std::string &id, NMLPackage::Mesh &nmlMesh) const;
		bool loadNMLMesh(const NMLPackage::Mesh &nmlMesh);

		/** Operators */
		size_t hash() const { return boost::hash_range(mSubmeshList.begin(), mSubmeshList.end()); }
		bool operator == (const Mesh &mesh) const { return mSubmeshList == mesh.mSubmeshList; }
		bool operator != (const Mesh &mesh) const { return !(*this == mesh); }

	private:

		SubmeshList mSubmeshList;
	};

	inline size_t hash_value(const Mesh &mesh)
	{
		return mesh.hash();
	}

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_MESH_H__
