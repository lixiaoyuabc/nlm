/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_MODEL_H__
#define __NMLFRAMEWORK_MODEL_H__

#include "Prerequisites.h"
#include "BaseTypes.h"
#include "MeshInstance.h"
#include "Mesh.h"
#include "Texture.h"
#include "LightingModel.h"

#include <list>


namespace NMLFramework
{

	class Model
	{

	public:

		typedef std::map<std::string, Texture> TextureMap;
		typedef std::map<std::string, Mesh> MeshMap;
		typedef std::list<MeshInstance> MeshInstanceList;

		/** Default constructor */
		Model() : mMeshFootprint(0), mTextureFootprint(0) {}

		/** Access to model attributes */
		const TextureMap &getTextureMap() const { return mTextureMap; }
		TextureMap &getTextureMap() { return mTextureMap; }
		Texture *getTexture(const std::string &id);
		const Texture *getTexture(const std::string &id) const;
		std::string storeTexture(const Texture &tex);
		int countTextureInstances(const std::string &id) const;
		const MeshMap &getMeshMap() const { return mMeshMap; }
		MeshMap &getMeshMap() { return mMeshMap; }
		Mesh *getMesh(const std::string &id);
		const Mesh *getMesh(const std::string &id) const;
		std::string storeMesh(const Mesh &mesh);
		int countMeshInstances(const std::string &id) const;
		const MeshInstanceList &getMeshInstanceList() const { return mMeshInstanceList; }
		MeshInstanceList &getMeshInstanceList() { return mMeshInstanceList; }
		int getVertexCount() const;
		int getMaterialCount() const;

		/** Update all vertex ids of this model */
		void setVertexIds(int id);

		/** Calculate bounds with given transform */
		Bounds3 calculateBounds(const Matrix4 &transform) const;

		/** Calculate model floorplan */
		std::vector<Vector3> calculateFloorplan(float &minHeight, float &maxHeight) const;

		/** Clear model */
		void clear();

		/** Concatenate another model */
		void concat(const Model &model);

		/** Resize all model textures */
		void resizeTextures(float scale);

		/** Resize all model textures to power-of-two size */
		void resizeTexturesToPOT(int maxSize);

		/** Strip points and lines, leave only triangles */
		void stripPointsLines();

		/** Calculate average colors for materials that use textures and store it in material */
		void calculateAvgColors();

		/** Calculate memory footprint */
		void calculateMemoryFootprint(const TextureExporter &textureExporter);

		/** Apply lighting model to submesh */
		void applyLightingModel(const LightingModel &lighting, const Matrix4 &transform);

		/** Merge materials with the same content */
		void mergeMaterials();

		/** Merge submeshes sharing the same material */
		void mergeSubmeshes();

		/** Split submeshes containing given number of vertices into separate mesh */
		void splitSubmeshes(int maxVertices);

		/** Generate vertex colors for materials with constant color. Such submeshes can be merged. */
		void generateVertexColors(int maxVertices);

		/** Generate texture atlas */
		void generateTextureAtlas(int maxTextureSize, int maxAtlasTextureSize, bool squareAtlas, int maxVertices, int maxRepeatedPixels);

		/** Flatten model - keep only single mesh, by concatenating all meshes */
		void flatten(int maxExtraVertices, int maxMeshVertices);

		/** Optimize model based on 'bounds size' */
		void optimizeForBoundsSize(const Matrix4 &transform, const Vector3 &boundsSize, float sizeThreshold, int screenSize, int maxTextureSize);

		/** Make all mesh,texture and material names canonical, this operation is idempotent */
		void canonizeNames();

		/** Cleanup model assets - remove unused textures/meshes */
		void cleanup();

		/** Export/import model as NMLModel */
		bool saveNMLModel(const std::string &id, NMLPackage::Model &nmlModel, const TextureExporter &textureExporter) const;
		bool loadNMLModel(const NMLPackage::Model &nmlModel);

	protected:

		TextureMap mTextureMap;
		MeshMap mMeshMap;
		MeshInstanceList mMeshInstanceList;
		int mMeshFootprint;
		int mTextureFootprint;
	};

} // namespace NMLFramework

#endif // __NMLFRAMEWORK_MODEL_H__
