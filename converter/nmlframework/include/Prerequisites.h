/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#ifndef __NMLFRAMEWORK_PREREQUISITES_H__
#define __NMLFRAMEWORK_PREREQUISITES_H__

#define NOMINMAX

#include <string>
#include <iostream>
#include <limits>
#include <stdexcept>

// Integer formats of fixed bit width
typedef unsigned int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;

#endif //__NMLFRAMEWORK_PREREQUISITES_H__
