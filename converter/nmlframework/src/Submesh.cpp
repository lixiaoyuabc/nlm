/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Submesh.h"

#include <numeric>

#include <boost/functional/hash.hpp>


namespace NMLFramework
{

	//--------------------------------------------------------------------
	void Submesh::setVertexIds(int id)
	{
		int vertexCount = std::accumulate(mVertexCounts.begin(), mVertexCounts.end(), 0);
		mVertexIds.assign(vertexCount, id);
	}

	//--------------------------------------------------------------------
	std::vector<Submesh::IntVector> Submesh::getPrimitiveIndices() const
	{
		std::vector<IntVector> indices;
		switch (mType)
		{
		case NMLPackage::Submesh::POINTS:
		case NMLPackage::Submesh::LINES:
		case NMLPackage::Submesh::TRIANGLES:
			{
				int idx = 0;
				for (size_t i = 0; i < mVertexCounts.size(); i++)
				{
					int count = (mType == NMLPackage::Submesh::POINTS ? 1 : (mType == NMLPackage::Submesh::LINES ? 2 : 3));
					for (int j = 0; j + count <= mVertexCounts[i]; j += count)
					{
						indices.push_back(IntVector());
						for (int k = 0; k < count; k++)
						{
							indices.back().push_back(idx++);
						}
					}
				}
			}
			break;
		case NMLPackage::Submesh::LINE_STRIPS:
			{
				int idx = 0;
				for (size_t i = 0; i < mVertexCounts.size(); i++)
				{
					indices.push_back(IntVector());
					for (int j = 1; j < mVertexCounts[i]; j++)
					{
						indices.back().push_back(idx + j - 1);
						indices.back().push_back(idx + j - 0);
					}
					idx += mVertexCounts[i];
				}
			}
			break;
		case NMLPackage::Submesh::TRIANGLE_STRIPS:
		case NMLPackage::Submesh::TRIANGLE_FANS:
			{
				int idx = 0;
				for (size_t i = 0; i < mVertexCounts.size(); i++)
				{
					indices.push_back(IntVector());
					for (int j = 2; j < mVertexCounts[i]; j++)
					{
						indices.back().push_back(mType == NMLPackage::Submesh::TRIANGLE_FANS ? 0 : idx + j - 2);
						indices.back().push_back(idx + j - 1);
						indices.back().push_back(idx + j - 0);
					}
					idx += mVertexCounts[i];
				}
			}
			break;
		}
		return indices;
	}

	//--------------------------------------------------------------------
	Bounds3 Submesh::calculateBounds(const Matrix4 &transform) const
	{
		// Calculate bounds
		Bounds3 bounds;
		for (size_t i = 0; i + 3 <= mPositions.size(); i += 3)
		{
			Vector3 pos(mPositions[i + 0], mPositions[i + 1], mPositions[i + 2]);
			Vector3 newPos = transform * pos;
			bounds.add(Vector3(newPos[0], newPos[1], newPos[2]));
		}
		return bounds;
	}

	//--------------------------------------------------------------------
	int Submesh::estimateMemoryFootprint() const
	{
		size_t size = mPositions.size() * sizeof(float);
		size += mNormals.size() * sizeof(float); // TODO: better encoding for normals?
		size += mUVs.size() * sizeof(float);
		size += mColors.size() * sizeof(unsigned char);
		if (!mVertexIds.empty())
			size += mPositions.size() * sizeof(unsigned char); // vertex ids when uncompressed
		return (int) size;
	}

	//--------------------------------------------------------------------
	float Submesh::calculatePixelTexelRatio(const Matrix4 &transform, double pixelSize, const Vector3 &textureSize) const
	{
		static const double EPSILON = 1.0e-10;

		double maxRatio = 0;
		std::vector<IntVector> indices = getPrimitiveIndices();
		for (size_t i = 0; i < indices.size(); i++)
		{
			for (size_t j = 1; j < indices[i].size(); j++)
			{
				int idx0 = indices[i][j - 1];
				int idx1 = indices[i][j];
				if (idx0*2 + 2 >= (int) mUVs.size() || idx1*2 + 2 >= (int) mUVs.size())
					break;
				Vector3 uv0(mUVs[idx0*2 + 0], mUVs[idx0*2 + 1], 0);
				Vector3 uv1(mUVs[idx1*2 + 0], mUVs[idx1*2 + 1], 0);
				Vector3 dUV(std::abs(uv1.x - uv0.x), std::abs(uv1.y - uv0.y), 0);
				if (dUV.length() < EPSILON)
					continue;
				dUV.x = dUV.x * textureSize.x;
				dUV.y = dUV.y * textureSize.y;
				Vector3 pos0(mPositions[idx0*3 + 0], mPositions[idx0*3 + 1], mPositions[idx0*3 + 2]);
				Vector3 pos1(mPositions[idx1*3 + 0], mPositions[idx1*3 + 1], mPositions[idx1*3 + 2]);
				Vector3 dPos = (transform * pos1 - transform * pos0);
				maxRatio = std::max(maxRatio, dPos.length() * pixelSize / std::max(dUV.x, dUV.y));
			}
		}
		return (float) maxRatio;
	}

	//--------------------------------------------------------------------
	void Submesh::applyTransformation(const Matrix4 &transform)
	{
		for (size_t i = 0; i + 3 <= mPositions.size(); i += 3)
		{
			Vector3 pos(mPositions[i + 0], mPositions[i + 1], mPositions[i + 2]);
			Vector3 newPos = transform * pos;
			mPositions[i + 0] = (float) newPos.x; mPositions[i + 1] = (float) newPos.y; mPositions[i + 2] = (float) newPos.z;
		}
	}

	//--------------------------------------------------------------------
	bool Submesh::concat(const Submesh &submesh)
	{
		// Check if combining is possible
		if (!isEmpty() && !submesh.isEmpty())
		{
			if (mType != submesh.mType)
				return false;
			if (mMaterialId != submesh.mMaterialId)
				return false;
		}
		else if (isEmpty())
		{
			mType = submesh.mType;
			mMaterialId = submesh.mMaterialId;
		}

		// Possible to combine
		mVertexCounts.insert(mVertexCounts.end(), submesh.mVertexCounts.begin(), submesh.mVertexCounts.end());
		switch (mType)
		{
		case NMLPackage::Submesh::POINTS:
		case NMLPackage::Submesh::LINES:
		case NMLPackage::Submesh::TRIANGLES:
			{
				int vertexCount = std::accumulate(mVertexCounts.begin(), mVertexCounts.end(), 0);
				mVertexCounts.assign(1, vertexCount);
			}
			break;
		default:
			break;
		}
		mPositions.insert(mPositions.end(), submesh.mPositions.begin(), submesh.mPositions.end());
		mNormals.insert(mNormals.end(), submesh.mNormals.begin(), submesh.mNormals.end());
		mUVs.insert(mUVs.end(), submesh.mUVs.begin(), submesh.mUVs.end());
		mColors.insert(mColors.end(), submesh.mColors.begin(), submesh.mColors.end());
		mVertexIds.insert(mVertexIds.end(), submesh.mVertexIds.begin(), submesh.mVertexIds.end());
		return true;
	}

	//--------------------------------------------------------------------
	bool Submesh::saveNMLSubmesh(NMLPackage::Submesh &nmlSubmesh) const
	{
		nmlSubmesh.set_type(mType);
		nmlSubmesh.set_material_id(mMaterialId);
		for (size_t i = 0; i < mVertexCounts.size(); i++)
		{
			nmlSubmesh.add_vertex_counts(mVertexCounts[i]);
		}
		*nmlSubmesh.mutable_positions() = convertToNetworkFloatVector(mPositions);
		if (!mNormals.empty())
			*nmlSubmesh.mutable_normals() = convertToNetworkFloatVector(mNormals); // TODO: better encoding (3 x signed byte) for normals?
		if (!mUVs.empty())
			*nmlSubmesh.mutable_uvs() = convertToNetworkFloatVector(mUVs);
		if (!mColors.empty())
			*nmlSubmesh.mutable_colors() = convertToNetworkByteVector(mColors);
		int count = 0; int vertexId = -1;
		for (size_t i = 0; i < mVertexIds.size(); i++)
		{
			if (mVertexIds[i] != vertexId)
			{
				if (count > 0)
					nmlSubmesh.add_vertex_ids(((long long) count << 32) | vertexId);
				vertexId = mVertexIds[i];
				count = 1;
			}
			else
			{
				count++;
			}
		}
		if (count > 0)
			nmlSubmesh.add_vertex_ids(((long long) count << 32) | vertexId);
		return true;
	}

	//--------------------------------------------------------------------
	bool Submesh::loadNMLSubmesh(const NMLPackage::Submesh &nmlSubmesh)
	{
		mType = nmlSubmesh.type();
		mMaterialId = nmlSubmesh.material_id();
		mVertexCounts.clear();
		for (int i = 0; i < nmlSubmesh.vertex_counts_size(); i++)
		{
			mVertexCounts.push_back(nmlSubmesh.vertex_counts(i));
		}
		mPositions.clear();
		if (nmlSubmesh.has_positions())
			mPositions = convertFromNetworkFloatVector(nmlSubmesh.positions());
		mNormals.clear();
		if (nmlSubmesh.has_normals())
			mNormals = convertFromNetworkFloatVector(nmlSubmesh.normals());
		mUVs.clear();
		if (nmlSubmesh.has_uvs())
			mUVs = convertFromNetworkFloatVector(nmlSubmesh.uvs());
		mColors.clear();
		if (nmlSubmesh.has_colors())
			mColors = convertFromNetworkByteVector(nmlSubmesh.colors());
		mVertexIds.clear();
		for (int i = 0; i < nmlSubmesh.vertex_ids_size(); i++)
		{
			int vertexId = (int) nmlSubmesh.vertex_ids(i);
			int count = (int) (nmlSubmesh.vertex_ids(i) >> 32);
			while (count-- > 0)
			{
				mVertexIds.push_back(vertexId);
			}
		}
		return true;
	}

	//--------------------------------------------------------------------
	size_t Submesh::hash() const
	{
		std::size_t seed = 0;
		boost::hash_combine(seed, (int) mType);
		boost::hash_combine(seed, mMaterialId);
		boost::hash_combine(seed, mVertexCounts);
		boost::hash_combine(seed, mPositions);
		boost::hash_combine(seed, mNormals);
		boost::hash_combine(seed, mUVs);
		boost::hash_combine(seed, mColors);
		boost::hash_combine(seed, mVertexIds);
		return seed;
	}

	//--------------------------------------------------------------------
	bool Submesh::operator == (const Submesh &submesh) const
	{
		return mType == submesh.mType && mMaterialId == submesh.mMaterialId && mVertexCounts == submesh.mVertexCounts && mPositions == submesh.mPositions && mNormals == submesh.mNormals && mUVs == submesh.mUVs && mColors == submesh.mColors && mVertexIds == submesh.mVertexIds;
	}

	//--------------------------------------------------------------------
	std::string Submesh::convertToNetworkByteVector(const std::vector<float> &data)
	{
		std::string netData(data.size() * 1, 0);
		for (size_t i = 0; i < data.size(); i++)
		{
			float val = std::max(0.0f, std::min(1.0f, data[i]));
			unsigned char *netBytePtr = reinterpret_cast<unsigned char *>(&netData[i]);
			*netBytePtr = (unsigned char) (val * 255.0f);
		}
		return netData;
	}

	//--------------------------------------------------------------------
	std::string Submesh::convertToNetworkFloatVector(const std::vector<float> &data)
	{
		std::string netData(data.size() * 4, 0);
		for (size_t i = 0; i < data.size(); i++)
		{
			// Do conversion manually, htonl is a platform-specific mess
			assert(sizeof(unsigned int) == 4);
			assert(sizeof(float) == 4);
			unsigned int val = *reinterpret_cast<const unsigned int *>(&data[i]);
			unsigned char *netBytePtr = reinterpret_cast<unsigned char *>(&netData[i * 4]);
			netBytePtr[0] = static_cast<unsigned char>((val >>  0) & 255);
			netBytePtr[1] = static_cast<unsigned char>((val >>  8) & 255);
			netBytePtr[2] = static_cast<unsigned char>((val >> 16) & 255);
			netBytePtr[3] = static_cast<unsigned char>((val >> 24) & 255);
		}
		return netData;
	}

	//--------------------------------------------------------------------
	std::vector<float> Submesh::convertFromNetworkByteVector(const std::string &netData)
	{
		std::vector<float> data(netData.size() / 1, 0);
		for (size_t i = 0; i < data.size(); i++)
		{
			unsigned char b = (unsigned char) netData[i];
			float *floatPtr = &data[i];
			*floatPtr = (float) b / 255.0f;
		}
		return data;
	}

	//--------------------------------------------------------------------
	std::vector<float> Submesh::convertFromNetworkFloatVector(const std::string &netData)
	{
		std::vector<float> data(netData.size() / 4, 0);
		for (size_t i = 0; i < data.size(); i++)
		{
			// Do conversion manually, htonl is a platform-specific mess
			assert(sizeof(unsigned int) == 4);
			assert(sizeof(float) == 4);
			unsigned int b0 = (unsigned char) netData[i * 4 + 0];
			unsigned int b1 = (unsigned char) netData[i * 4 + 1];
			unsigned int b2 = (unsigned char) netData[i * 4 + 2];
			unsigned int b3 = (unsigned char) netData[i * 4 + 3];
			unsigned int *floatPtr = reinterpret_cast<unsigned int *>(&data[i]);
			*floatPtr = (b0 << 0) + (b1 << 8) + (b2 << 16) + (b3 << 24);
		}
		return data;
	}

} // namespace NMLFramework
