/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Processor.h"
#include "Utils.h"


namespace NMLFramework
{

	//--------------------------------------------------------------------
	void Processor::reportError(const std::string &method, const std::string& message, Severity severity) const
	{
		switch (severity)
		{
		case SEVERITY_INFORMATION:
			std::cout << "Information"; 
			break;
		case SEVERITY_WARNING:
			std::cout << "Warning"; 
			break;
		case _SEVERITY_ERROR:
			std::cout << "Error"; 
			break;
		}
		if (!method.empty())
			std::cout << " in " << method;
		std::cout << ": ";
		std::cout << message << std::endl;
	}

	//--------------------------------------------------------------------
	void Processor::reportProgress(const std::string& method, const std::string& message) const
	{
		std::cout << message << std::endl;
	}

} // namespace NMLFramework
