/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "MeshInstance.h"
#include "Utils.h"


namespace NMLFramework
{

	//--------------------------------------------------------------------
	Material *MeshInstance::getMaterial(const std::string &id)
	{
		MaterialMap::iterator it = mMaterialMap.find(id);
		if (it == mMaterialMap.end())
			return 0;
		return &it->second;
	}

	//--------------------------------------------------------------------
	const Material *MeshInstance::getMaterial(const std::string &id) const
	{
		MaterialMap::const_iterator it = mMaterialMap.find(id);
		if (it == mMaterialMap.end())
			return 0;
		return &it->second;
	}

	//--------------------------------------------------------------------
	std::string MeshInstance::storeMaterial(const Material &material)
	{
		std::string id = findUniqueId("M", mMaterialMap);
		mMaterialMap[id] = material;
		return id;
	}

	//--------------------------------------------------------------------
	bool MeshInstance::saveNMLMeshInstance(NMLPackage::MeshInstance &nmlMeshInstance) const
	{
		nmlMeshInstance.set_mesh_id(mMeshId);
		if (mTransform != Matrix4::IDENTITY)
			*nmlMeshInstance.mutable_transform() = mTransform.getNMLMatrix();
		for (MaterialMap::const_iterator it = mMaterialMap.begin(); it != mMaterialMap.end(); it++)
		{
			it->second.saveNMLMaterial(it->first, *nmlMeshInstance.add_materials());
		}
		return true;
	}

	//--------------------------------------------------------------------
	bool MeshInstance::loadNMLMeshInstance(const NMLPackage::MeshInstance &nmlMeshInstance)
	{
		mMeshId = nmlMeshInstance.mesh_id();
		mTransform = Matrix4::IDENTITY;
		if (nmlMeshInstance.has_transform())
			mTransform = Matrix4::fromNMLMatrix(nmlMeshInstance.transform());
		for (int i = 0; i < nmlMeshInstance.materials_size(); i++)
		{
			const NMLPackage::Material &nmlMaterial = nmlMeshInstance.materials(i);
			Material material;
			material.loadNMLMaterial(nmlMaterial);
			mMaterialMap[nmlMaterial.id()] = material;
		}
		return true;
	}

} // namespace NMLFramework
