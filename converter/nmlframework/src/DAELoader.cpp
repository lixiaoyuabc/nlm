/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "DAELoader.h"
#include "Triangulator.h"

#include "COLLADASaxFWLLoader.h"
#include "COLLADAFWTranslate.h"
#include "COLLADAFWRotate.h"
#include "COLLADAFWScale.h"
#include "COLLADAFWFileInfo.h"
#include "COLLADAFWIndexList.h"


namespace NMLFramework
{

	//--------------------------------------------------------------------
	bool DAELoader::ExtraCallbackHandler::elementBegin(const COLLADASaxFWL::ParserChar* elementName, const GeneratedSaxParser::xmlChar** attributes)
	{
		if (std::string(elementName) == "double_sided")
			return true;
		return false;
	}

	//--------------------------------------------------------------------
	bool DAELoader::ExtraCallbackHandler::elementEnd(const COLLADASaxFWL::ParserChar* elementName)
	{
		mUniqueId = COLLADAFW::UniqueId::INVALID;
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::ExtraCallbackHandler::textData(const COLLADASaxFWL::ParserChar* text, size_t textLength)
	{
		if (mUniqueId != COLLADAFW::UniqueId::INVALID)
			mWriter->mDoubleSided[mUniqueId] = std::string(text, textLength) == "1";
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::ExtraCallbackHandler::parseElement(const COLLADASaxFWL::ParserChar* profileName, const COLLADASaxFWL::StringHash& elementHash, const COLLADAFW::UniqueId& uniqueId)
	{
		if (std::string(profileName) == "GOOGLEEARTH")
		{
			mUniqueId = uniqueId;
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	DAELoader::DAELoader(const COLLADABU::URI& inputUri, URILoader& loader, const COLLADABU::Math::Matrix4 &rootTransform, bool loadTextures, Model &model)
		: mInputUri(inputUri), mPass(PASS_COLLECT), mLoadTextures(loadTextures), mURILoader(loader), mRootTransform(rootTransform), mUnitInMeters(1), mAxisTransform(Matrix4::IDENTITY), mModel(model)
	{
	}

	//--------------------------------------------------------------------
	DAELoader::~DAELoader()
	{
	}

	//--------------------------------------------------------------------
	std::string DAELoader::getStringId(const COLLADAFW::UniqueId &id) const
	{
		std::stringstream stream;
		stream << "U" << id.getClassId() << "#" << id.getObjectId();
		return stream.str();
	}

	//--------------------------------------------------------------------
	std::string DAELoader::getMaterialId(const COLLADAFW::MaterialId &id) const
	{
		std::stringstream stream;
		stream << "M" << id;
		return stream.str();
	}

	//--------------------------------------------------------------------
	std::string DAELoader::getTextureId(const COLLADAFW::Texture *colladaTexture, const COLLADAFW::SamplerPointerArray &colladaSamplers)
	{
		Texture texture;
		const COLLADAFW::Sampler *colladaSampler = colladaSamplers[colladaTexture->getSamplerId()];
		texture.setSampler(convertSampler(colladaSampler));
		UniqueIdImageMap::const_iterator it = mUniqueIdImageMap.find(colladaSampler->getSourceImage());
		if (it != mUniqueIdImageMap.end())
		{
			texture.setImage(it->second);
		}
		else
		{
			reportError("getTextureId", "Unable to find texture", SEVERITY_WARNING);
		}

		// Try to reuse existing texture
		for (Model::TextureMap::const_iterator it = mModel.getTextureMap().begin(); it != mModel.getTextureMap().end(); it++)
		{
			if (it->second == texture)
				return it->first;
		}

		// Generate new reference
		std::stringstream stream;
		stream << "T" << mModel.getTextureMap().size();
		std::string id = stream.str();
		mModel.getTextureMap()[id] = texture;
		return id;
	}

	//--------------------------------------------------------------------
	std::string DAELoader::getMeshId(const COLLADAFW::InstanceGeometry &instanceGeom)
	{
		UniqueIdMeshMap::const_iterator it = mUniqueIdMeshMap.find(instanceGeom.getInstanciatedObjectId());
		if (it == mUniqueIdMeshMap.end())
		{
			return ""; // do not warn, some geometry instances are not meshes
		}
		std::string id = getStringId(instanceGeom.getInstanciatedObjectId());
		mModel.getMeshMap()[id] = it->second;
		return id;
	}

	//--------------------------------------------------------------------
	std::vector<float> DAELoader::convertToFloatVector(const COLLADAFW::MeshVertexData &data) const
	{
		std::vector<float> vector(data.getValuesCount());
		for (size_t i = 0; i < vector.size(); i++)
		{
			float val = 0;
			switch (data.getType())
			{
			case COLLADAFW::MeshVertexData::DATA_TYPE_FLOAT:
				val = (*data.getFloatValues())[i];
				break;
			case COLLADAFW::MeshVertexData::DATA_TYPE_DOUBLE:
				val = static_cast<float>((*data.getDoubleValues())[i]);
				break;
			}
			vector[i] = val;
		}
		return vector;
	}

	//--------------------------------------------------------------------
	NMLPackage::Vector3 DAELoader::convertVector3(const COLLADABU::Math::Vector3 &colladaVector) const
	{
		return Vector3(colladaVector).getNMLVector();
	}

	//--------------------------------------------------------------------
	NMLPackage::Matrix4 DAELoader::convertMatrix4(const COLLADABU::Math::Matrix4 &colladaMatrix) const
	{
		return Matrix4(colladaMatrix).getNMLMatrix();
	}

	//--------------------------------------------------------------------
	NMLPackage::ColorRGBA DAELoader::convertColor(const COLLADAFW::Color *colladaColor) const
	{
		if (colladaColor == 0)
			return NMLPackage::ColorRGBA();
		return ColorRGBA(*colladaColor).getNMLColor();
	}

	//--------------------------------------------------------------------
	NMLPackage::Sampler DAELoader::convertSampler(const COLLADAFW::Sampler *colladaSampler) const
	{
		NMLPackage::Sampler::Filter nmlFilter = NMLPackage::Sampler::TRILINEAR;
		switch (colladaSampler->getMagFilter())
		{
		case COLLADAFW::Sampler::SAMPLER_FILTER_NEAREST:
			nmlFilter = NMLPackage::Sampler::NEAREST;
			break;
		default:
			if (colladaSampler->getMipFilter() == COLLADAFW::Sampler::SAMPLER_FILTER_NEAREST)
				nmlFilter = NMLPackage::Sampler::BILINEAR;
			else
				nmlFilter = NMLPackage::Sampler::TRILINEAR;
			break;
		}
		COLLADAFW::Sampler::WrapMode wrapModes[3] = { colladaSampler->getWrapS(), colladaSampler->getWrapT(), colladaSampler->getWrapP() };
		NMLPackage::Sampler::WrapMode nmlWrapMode[3] = { NMLPackage::Sampler::CLAMP, NMLPackage::Sampler::CLAMP, NMLPackage::Sampler::CLAMP };
		for (int i = 0; i < 3; i++)
		{
			switch (wrapModes[i])
			{
			case COLLADAFW::Sampler::WRAP_MODE_CLAMP:
				nmlWrapMode[i] = NMLPackage::Sampler::CLAMP;
				break;
			case COLLADAFW::Sampler::WRAP_MODE_MIRROR:
				nmlWrapMode[i] = NMLPackage::Sampler::MIRROR;
				break;
			default:
				nmlWrapMode[i] = NMLPackage::Sampler::REPEAT;
				break;
			}
		}
		NMLPackage::Sampler nmlSampler;
		nmlSampler.set_filter(nmlFilter);
		nmlSampler.set_wrap_s(nmlWrapMode[0]);
		nmlSampler.set_wrap_t(nmlWrapMode[1]);
		return nmlSampler;
	}

	//--------------------------------------------------------------------
	NMLPackage::ColorOrTexture DAELoader::convertColorOrTexture(const COLLADAFW::ColorOrTexture *colladaColorOrTexture, const COLLADAFW::SamplerPointerArray &colladaSamplers, float translucency)
	{
		NMLPackage::ColorOrTexture nmlColorOrTexture;
		switch (colladaColorOrTexture->getType())
		{
		case COLLADAFW::ColorOrTexture::COLOR:
			nmlColorOrTexture.set_type(NMLPackage::ColorOrTexture::COLOR);
			*nmlColorOrTexture.mutable_color() = convertColor(&colladaColorOrTexture->getColor());
			nmlColorOrTexture.mutable_color()->set_a(1); // HACK: set alpha explicitly to 1 to make material non-transparent
			if (translucency < 1)
			{
				nmlColorOrTexture.mutable_color()->set_r(nmlColorOrTexture.color().r() * translucency);
				nmlColorOrTexture.mutable_color()->set_g(nmlColorOrTexture.color().g() * translucency);
				nmlColorOrTexture.mutable_color()->set_b(nmlColorOrTexture.color().b() * translucency);
				nmlColorOrTexture.mutable_color()->set_a(nmlColorOrTexture.color().a() * translucency);
			}
			break;
		case COLLADAFW::ColorOrTexture::TEXTURE:
			nmlColorOrTexture.set_type(NMLPackage::ColorOrTexture::TEXTURE);
			nmlColorOrTexture.set_texture_id(getTextureId(&colladaColorOrTexture->getTexture(), colladaSamplers));
			if (translucency < 1)
			{
				nmlColorOrTexture.mutable_color()->set_r(translucency);
				nmlColorOrTexture.mutable_color()->set_g(translucency);
				nmlColorOrTexture.mutable_color()->set_b(translucency);
				nmlColorOrTexture.mutable_color()->set_a(translucency);
			}
			break;
		}
		return nmlColorOrTexture;
	}

	//--------------------------------------------------------------------
	bool DAELoader::createMaterial(const COLLADAFW::MaterialBinding *colladaMaterialBinding, const COLLADAFW::Material *colladaMaterial, Material &material)
	{
		static const float TRANSLUCENCY_THRESHOLD = 0.95f;

		// Find effect corresponding to material
		UniqueIdFWEffectMap::const_iterator it = mUniqueIdFWEffectMap.find(colladaMaterial->getInstantiatedEffect());
		if (it == mUniqueIdFWEffectMap.end())
		{
			reportError("createMaterial", "Unable to find effect for material", SEVERITY_WARNING);
			return false;
		}
		const COLLADAFW::Effect &colladaEffect = it->second;
		const COLLADAFW::CommonEffectPointerArray &colladaCommonEffects = colladaEffect.getCommonEffects();
		if (colladaCommonEffects.empty())
		{
			reportError("createMaterial", "Material common effect profile is missing", SEVERITY_WARNING);
			return false;
		}
		const COLLADAFW::EffectCommon &colladaCommonEffect = *colladaCommonEffects[0];

		// Set material parameters
		float translucency = 1;
		if (colladaCommonEffect.getOpacity().isColor())
		{
			const COLLADAFW::Color &colladaColor = colladaCommonEffect.getOpacity().getColor();
			float opacity1 = std::min(std::min(colladaColor.getRed(), colladaColor.getGreen()), colladaColor.getBlue());
			float opacity2 = std::min(std::min(1 - colladaColor.getRed(), 1 - colladaColor.getGreen()), 1 - colladaColor.getBlue());
			translucency = std::max(opacity1, opacity2); // rework bug in some DAE models, better than hiding models
		}
		material.setType(NMLPackage::Material::LAMBERT);
		material.setTranslucent(translucency < TRANSLUCENCY_THRESHOLD);

		NMLPackage::ColorOrTexture nmlEmission = convertColorOrTexture(&colladaCommonEffect.getEmission(), colladaCommonEffect.getSamplerPointerArray(), translucency);
		if (nmlEmission.has_type())
			material.setColorOrTexture(Material::EMISSION, nmlEmission);
		NMLPackage::ColorOrTexture nmlAmbient = convertColorOrTexture(&colladaCommonEffect.getAmbient(), colladaCommonEffect.getSamplerPointerArray(), translucency);
		if (nmlAmbient.has_type())
			material.setColorOrTexture(Material::AMBIENT, nmlAmbient);
		NMLPackage::ColorOrTexture nmlDiffuse = convertColorOrTexture(&colladaCommonEffect.getDiffuse(), colladaCommonEffect.getSamplerPointerArray(), translucency);
		if (nmlDiffuse.has_type())
			material.setColorOrTexture(Material::DIFFUSE, nmlDiffuse);

		material.setCulling(NMLPackage::Material::BACK);
		if (mDoubleSided.count(colladaEffect.getUniqueId()))
		{
			if (mDoubleSided[colladaEffect.getUniqueId()])
				material.setCulling(NMLPackage::Material::NONE);
		}
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::createSubmesh(const COLLADAFW::Mesh *colladaMesh, const COLLADAFW::MeshPrimitive *colladaMeshPrimitive, Submesh &submesh)
	{
		// Set primitive type
		NMLPackage::Submesh::Type type = NMLPackage::Submesh::POINTS;
		switch (colladaMeshPrimitive->getPrimitiveType())
		{
		case COLLADAFW::MeshPrimitive::POINTS:
			type = NMLPackage::Submesh::POINTS;
			break;
		case COLLADAFW::MeshPrimitive::LINES:
			type = NMLPackage::Submesh::LINES;
			break;
		case COLLADAFW::MeshPrimitive::LINE_STRIPS:
			type = NMLPackage::Submesh::LINE_STRIPS;
			break;
		case COLLADAFW::MeshPrimitive::TRIANGLES:
			type = NMLPackage::Submesh::TRIANGLES;
			break;
		case COLLADAFW::MeshPrimitive::TRIANGLE_STRIPS:
			type = NMLPackage::Submesh::TRIANGLE_STRIPS;
			break;
		case COLLADAFW::MeshPrimitive::TRIANGLE_FANS:
			type = NMLPackage::Submesh::TRIANGLE_FANS;
			break;
		case COLLADAFW::MeshPrimitive::POLYGONS:
		case COLLADAFW::MeshPrimitive::POLYLIST:
			type = NMLPackage::Submesh::TRIANGLES;
			break;
		default: 
			reportError("convertSubmesh", "Unsupported mesh primitive type", SEVERITY_WARNING);
			return false;
		}

		// Set basic properties
		submesh.setType(type);
		submesh.setMaterialId(getMaterialId(colladaMeshPrimitive->getMaterialId()));

		// Get vertex arrays
		const COLLADAFW::UIntValuesArray &positionIndices = colladaMeshPrimitive->getPositionIndices();
		size_t positionIndicesCount = positionIndices.getCount();

		const COLLADAFW::UIntValuesArray &normalIndices = colladaMeshPrimitive->getNormalIndices();
		size_t normalIndicesCount = normalIndices.getCount();
		bool hasNormals = (normalIndicesCount != 0);

		const COLLADAFW::UIntValuesArray *uvIndices = 0;
		size_t uvIndicesCount = 0;
		bool hasUVCoords = false;
		const COLLADAFW::IndexListArray &uvIndicesList = colladaMeshPrimitive->getUVCoordIndicesArray();
		if (!uvIndicesList.empty()) // NOTE: only use coordinate set 0
		{
			uvIndices = &uvIndicesList[0]->getIndices();
			uvIndicesCount = uvIndices->getCount();
			hasUVCoords = (uvIndicesCount != 0);
		}

		// Check normal/uv info consistency
		if (hasNormals && positionIndicesCount != normalIndicesCount)
		{
			reportError("convertSubmesh", "Inconsistent vertex and normal count", SEVERITY_WARNING);
			hasNormals = false;
		}
		if (hasUVCoords && positionIndicesCount != uvIndicesCount)
		{
			reportError("convertSubmesh", "Inconsistent vertex and uv count", SEVERITY_WARNING);
			hasUVCoords = false;
		}

		// Convert vertex info
		std::vector<float> colladaPositions = convertToFloatVector(colladaMesh->getPositions());
		std::vector<float> colladaNormals = convertToFloatVector(colladaMesh->getNormals());
		std::vector<float> colladaUVs = convertToFloatVector(colladaMesh->getUVCoords());
		Submesh::FloatVector &positions = submesh.getPositions();
		Submesh::FloatVector &normals = submesh.getNormals();
		Submesh::FloatVector &uvs = submesh.getUVs();
		switch (colladaMeshPrimitive->getPrimitiveType())
		{
		case COLLADAFW::MeshPrimitive::POINTS:
		case COLLADAFW::MeshPrimitive::LINES:
		case COLLADAFW::MeshPrimitive::LINE_STRIPS:
		case COLLADAFW::MeshPrimitive::TRIANGLES:
		case COLLADAFW::MeshPrimitive::TRIANGLE_STRIPS:
		case COLLADAFW::MeshPrimitive::TRIANGLE_FANS:
			{
				for (size_t j = 0; j < positionIndicesCount; ++j)
				{
					unsigned int positionIndex = positionIndices[j];
					if (positionIndex * 3 + 3 > colladaPositions.size())
					{
						reportError("convertSubmesh", "Mesh position index out of range", SEVERITY_WARNING);
						return false;
					}
					const float *colladaPosition = &colladaPositions[positionIndex * 3];
					positions.insert(positions.end(), colladaPosition, colladaPosition + 3);

					if (hasNormals)
					{
						unsigned int normalIndex = normalIndices[j];
						if (normalIndex * 3 + 3 > colladaNormals.size())
						{
							reportError("convertSubmesh", "Mesh normal index out of range", SEVERITY_WARNING);
							return false;
						}
						const float *colladaNormal = &colladaNormals[normalIndex * 3];
						normals.insert(normals.end(), colladaNormal, colladaNormal + 3);
					}

					if (hasUVCoords)
					{
						unsigned int uvIndex = (*uvIndices)[j];
						if (uvIndex * 2 + 2 > colladaUVs.size())
						{
							reportError("convertSubmesh", "Mesh uv index out of range", SEVERITY_WARNING);
							return false;
						}
						const float *colladaUV = &colladaUVs[uvIndex * 2];
						uvs.insert(uvs.end(), colladaUV, colladaUV + 2);
					}
				}
			}
			break;
		case COLLADAFW::MeshPrimitive::POLYGONS:
		case COLLADAFW::MeshPrimitive::POLYLIST:
			{
				const COLLADAFW::Polygons *polygons = static_cast<const COLLADAFW::Polygons *>(colladaMeshPrimitive);
				size_t idx = 0;
				bool hasHoles = false, isComplex = false;
				for (size_t i = 0; i < polygons->getFaceCount(); i++)
				{
					int count = polygons->getGroupedVerticesVertexCount(i);
					if (count < 0) // NOTE: count can be negative; in that case this is a hole in previous polygon
					{
						hasHoles = true; // warn later
						idx += -count;
						continue;
					}

					std::vector<float> triPositions;
					std::vector<int> inIndices, outIndices;
					for (int v = 0; v < count; v++)
					{
						unsigned int positionIndex = positionIndices[idx + v];
						const float *colladaPosition = &colladaPositions[positionIndex * 3];
						triPositions.insert(triPositions.end(), colladaPosition, colladaPosition + 3);
						inIndices.push_back(v);
					}
					Triangulator triangulator(&triPositions[0]);
					if (!triangulator.triangulate(inIndices, outIndices))
					{
						isComplex = true; // warn later
					}
					for (size_t k = 0; k < outIndices.size(); k++)
					{
						size_t j = idx + outIndices[k];
						if (j >= positionIndices.getCount())
						{
							reportError("convertSubmesh", "Mesh vertex index out of range", SEVERITY_WARNING);
							return false;
						}

						unsigned int positionIndex = positionIndices[j];
						if (positionIndex * 3 + 3 > colladaPositions.size())
						{
							reportError("convertSubmesh", "Mesh position index out of range", SEVERITY_WARNING);
							return false;
						}
						const float *colladaPosition = &colladaPositions[positionIndex * 3];
						positions.insert(positions.end(), colladaPosition, colladaPosition + 3);

						if (hasNormals)
						{
							unsigned int normalIndex = normalIndices[j];
							if (normalIndex * 3 + 3 > colladaNormals.size())
							{
								reportError("convertSubmesh", "Mesh normal index out of range", SEVERITY_WARNING);
								return false;
							}
							const float *colladaNormal = &colladaNormals[normalIndex * 3];
							normals.insert(normals.end(), colladaNormal, colladaNormal + 3);
						}

						if (hasUVCoords)
						{
							unsigned int uvIndex = (*uvIndices)[j];
							if (uvIndex * 2 + 2 > colladaUVs.size())
							{
								reportError("convertSubmesh", "Mesh uv index out of range", SEVERITY_WARNING);
								return false;
							}
							const float *colladaUV = &colladaUVs[uvIndex * 2];
							uvs.insert(uvs.end(), colladaUV, colladaUV + 2);
						}
					}
					idx += count;
				}
				if (hasHoles)
					reportError("convertSubmesh", "Holes in polygons are not supported", SEVERITY_WARNING);
				if (isComplex)
					reportError("convertSubmesh", "Mesh contains complex polygons that were not converted", SEVERITY_WARNING);
			}
			break;
		default:
			assert(false);
			break;
		}

		// Set vertex counts
		Submesh::IntVector &vertexCounts = submesh.getVertexCounts();
		switch (type)
		{
		case NMLPackage::Submesh::LINE_STRIPS:
		case NMLPackage::Submesh::TRIANGLE_STRIPS:
		case NMLPackage::Submesh::TRIANGLE_FANS:
			{
				const COLLADAFW::MeshPrimitiveWithFaceVertexCount<unsigned int> *colladaVCMeshPrimitive = static_cast<const COLLADAFW::MeshPrimitiveWithFaceVertexCount<unsigned int> *>(colladaMeshPrimitive);
				for (size_t i = 0; i < colladaVCMeshPrimitive->getGroupedVerticesVertexCountArray().getCount(); i++)
				{
					vertexCounts.push_back(colladaVCMeshPrimitive->getGroupedVerticesVertexCountArray()[i]);
				}
			}
			break;
		default:
			vertexCounts.push_back(positions.size() / 3);
			break;
		}
		return true;
	}

	//--------------------------------------------------------------------
	void DAELoader::createMeshInstances(const COLLADAFW::Node *colladaNode, const Matrix4 &parentMatrix)
	{
		const Matrix4 nodeMatrix = parentMatrix * colladaNode->getTransformationMatrix();

		const COLLADAFW::NodePointerArray &colladaChildNodes = colladaNode->getChildNodes();
		for (size_t i = 0, count = colladaChildNodes.getCount(); i < count; i++)
		{
			createMeshInstances(colladaChildNodes[i], nodeMatrix);
		}
		const COLLADAFW::InstanceNodePointerArray &colladaInstanceNodes = colladaNode->getInstanceNodes();
		for (size_t i = 0, count = colladaInstanceNodes.getCount(); i < count; i++)
		{
			const COLLADAFW::UniqueId& referencedNodeUniqueId = colladaInstanceNodes[i]->getInstanciatedObjectId();
			UniqueIdNodeMap::const_iterator it = mUniqueIdNodeMap.find(referencedNodeUniqueId);
			if (it != mUniqueIdNodeMap.end())
			{
				createMeshInstances(it->second, nodeMatrix);
			}
			else
			{
				reportError("convertNode", "Node is missing", SEVERITY_WARNING);
			}
		}

		const COLLADAFW::InstanceGeometryPointerArray &colladaGeoms = colladaNode->getInstanceGeometries();
		for (size_t i = 0, count = colladaGeoms.getCount(); i < count; i++)
		{
			const COLLADAFW::InstanceGeometry &instanceGeom = *colladaGeoms[i];
			std::string meshId = getMeshId(instanceGeom);
			if (meshId.empty())
				continue;

			MeshInstance meshInstance;
			meshInstance.setMeshId(meshId);
			meshInstance.setTransform(nodeMatrix);
			for (size_t j = 0; j < instanceGeom.getMaterialBindings().getCount(); j++)
			{
				Material material;
				const COLLADAFW::MaterialBinding &colladaMaterialBinding = instanceGeom.getMaterialBindings()[j];
				UniqueIdFWMaterialMap::const_iterator it = mUniqueIdFWMaterialMap.find(colladaMaterialBinding.getReferencedMaterial());
				if (it != mUniqueIdFWMaterialMap.end())
				{
					createMaterial(&colladaMaterialBinding, &it->second, material);
				}
				else
				{
					reportError("convertNode", "Material is missing", SEVERITY_WARNING);
				}
				std::string materialId = getMaterialId(colladaMaterialBinding.getMaterialId());
				meshInstance.getMaterialMap()[materialId] = material;
			}
			mModel.getMeshInstanceList().push_back(meshInstance);
		}
	}

	//--------------------------------------------------------------------
	void DAELoader::createUniqueIdNodeMap(COLLADAFW::Node *node)
	{
		mUniqueIdNodeMap[node->getUniqueId()] = node;
		createUniqueIdNodeMap(node->getChildNodes());
	}

	//--------------------------------------------------------------------
	void DAELoader::createUniqueIdNodeMap(const COLLADAFW::NodePointerArray &nodes)
	{
		for (size_t i = 0, count = nodes.getCount(); i < count; ++i)
		{
			createUniqueIdNodeMap(nodes[i]);
		}
	}

	//--------------------------------------------------------------------
	void DAELoader::createUniqueIdNodeMap()
	{
		for (VisualSceneList::const_iterator it = mVisualSceneList.begin(); it != mVisualSceneList.end(); it++)
		{
			const COLLADAFW::NodePointerArray &rootNodes = it->getRootNodes();
			createUniqueIdNodeMap(rootNodes);
		}
		for (DAELoader::LibraryNodesList::const_iterator it = mLibraryNodesList.begin(); it != mLibraryNodesList.end(); ++it)
		{
			const COLLADAFW::LibraryNodes &libraryNodes = *it;
			createUniqueIdNodeMap(libraryNodes.getNodes());
		}
	}

	//--------------------------------------------------------------------
	bool DAELoader::load()
	{
		COLLADASaxFWL::Loader loader;
		ExtraCallbackHandler extraHandler(this);
		loader.registerExtraDataCallbackHandler(&extraHandler);
		COLLADAFW::Root root(&loader, this);

		// Clear lists/maps
		mModel.clear();

		mUnitInMeters = 1;
		mVisualSceneList.clear();
		mLibraryNodesList.clear();
		mUniqueIdNodeMap.clear();
		mUniqueIdFWMaterialMap.clear();
		mUniqueIdFWEffectMap.clear();
		mUniqueIdImageMap.clear();
		mUniqueIdMeshMap.clear();
		mDoubleSided.clear();

		// Load resources
		std::string fileName = mURILoader.resolve(mInputUri);

		mPass = PASS_COLLECT;
		if (!root.loadDocument(fileName))
			return false;

		mPass = PASS_CONVERT;
		if (!root.loadDocument(fileName))
			return false;

		// Convert scene nodes
		createUniqueIdNodeMap();
		for (VisualSceneList::const_iterator it = mVisualSceneList.begin(); it != mVisualSceneList.end(); it++)
		{
			const COLLADAFW::NodePointerArray &rootNodes = it->getRootNodes();
			for (size_t i = 0, count = rootNodes.getCount(); i < count; i++)
			{
				Matrix4 transform = mRootTransform * Matrix4::getScale(mUnitInMeters, mUnitInMeters, mUnitInMeters) * mAxisTransform;
				createMeshInstances(rootNodes[i], transform);
			}
		}
		return true;
	}

	//--------------------------------------------------------------------
	void DAELoader::cancel(const std::string &errorMessage)
	{
	}

	//--------------------------------------------------------------------
	void DAELoader::start()
	{
	}

	//--------------------------------------------------------------------
	void DAELoader::finish()
	{
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeGlobalAsset(const COLLADAFW::FileInfo* asset)
	{
		mUnitInMeters = asset->getUnit().getLinearUnitMeter();
		mAxisTransform = Matrix4::IDENTITY;
		switch (asset->getUpAxisType())
		{
		case COLLADAFW::FileInfo::X_UP:
			// Swap X/Z
			mAxisTransform.setElement(0, 0, 0);
			mAxisTransform.setElement(0, 2, 1);
			mAxisTransform.setElement(2, 0, -1);
			mAxisTransform.setElement(2, 2, 0);
			break;
		case COLLADAFW::FileInfo::Y_UP:
			// Swap Y/Z
			mAxisTransform.setElement(0, 0, -1);
			mAxisTransform.setElement(1, 1, 0);
			mAxisTransform.setElement(1, 2, 1);
			mAxisTransform.setElement(2, 1, 1);
			mAxisTransform.setElement(2, 2, 0);
			break;
		case COLLADAFW::FileInfo::Z_UP:
			// Nothing to switch
			break;
		default:
			break; // assume Z_UP
		}
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeVisualScene(const COLLADAFW::VisualScene* visualScene)
	{
		if (mPass != PASS_COLLECT)
			return true;

		mVisualSceneList.push_back(*visualScene);
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeScene(const COLLADAFW::Scene* scene)
	{
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeLibraryNodes(const COLLADAFW::LibraryNodes* libraryNodes)
	{
		if (mPass != PASS_COLLECT)
			return true;

		mLibraryNodesList.push_back(*libraryNodes);
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeGeometry(const COLLADAFW::Geometry* geometry)
	{
		if (mPass != PASS_CONVERT)
			return true;

		// Check geometry type
		const COLLADAFW::Mesh *colladaMesh = 0;
		switch (geometry->getType())
		{
		case COLLADAFW::Geometry::GEO_TYPE_CONVEX_MESH:
		case COLLADAFW::Geometry::GEO_TYPE_MESH:
			colladaMesh = static_cast<const COLLADAFW::Mesh *>(geometry);
			break;
		default:
			reportError("convertTransform", "Unsupported geometry type", SEVERITY_WARNING);
			return true;
		}
	
		// Create mesh
		Mesh mesh;
		const COLLADAFW::MeshPrimitiveArray& meshPrimitives = colladaMesh->getMeshPrimitives();
		for (size_t i = 0, count = meshPrimitives.getCount(); i < count; ++i)
		{
			Submesh submesh;
			if (createSubmesh(colladaMesh, meshPrimitives[i], submesh))
				mesh.getSubmeshList().push_back(submesh);
		}
		mUniqueIdMeshMap[colladaMesh->getUniqueId()] = mesh;
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeMaterial(const COLLADAFW::Material* material)
	{
		if (mPass != PASS_COLLECT)
			return true;

		mUniqueIdFWMaterialMap.insert(std::make_pair(material->getUniqueId(),*material));
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeEffect(const COLLADAFW::Effect* effect)
	{
		if (mPass != PASS_COLLECT)
			return true;

		mUniqueIdFWEffectMap.insert(std::make_pair(effect->getUniqueId(),*effect));
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeCamera(const COLLADAFW::Camera* camera)
	{
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeImage(const COLLADAFW::Image* colladaImage)
	{
		if (mPass != PASS_CONVERT)
			return true;

		// Load image
		Image image;
		if (mLoadTextures)
		{
			if (!image.load(colladaImage->getImageURI(), mURILoader))
			{
				reportError("writeImage", "Could not load image " + colladaImage->getImageURI().originalStr(), _SEVERITY_ERROR);
			}
		}
		mUniqueIdImageMap[colladaImage->getUniqueId()] = image;
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeLight(const COLLADAFW::Light* light)
	{
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeAnimation(const COLLADAFW::Animation* animation)
	{
		reportError("writeAnimation", "Animations are not supported", SEVERITY_WARNING);
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeAnimationList(const COLLADAFW::AnimationList* animationList)
	{
		reportError("writeAnimationList", "Animation lists are not supported", SEVERITY_WARNING);
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeSkinControllerData(const COLLADAFW::SkinControllerData* skinControllerData)
	{
		reportError("writeSkinControllerData", "Skinning is not supported", SEVERITY_WARNING);
		return true;
	}

	//--------------------------------------------------------------------
	bool DAELoader::writeController(const COLLADAFW::Controller* Controller)
	{
		reportError("writeController", "Controllers are not supported", SEVERITY_WARNING);
		return true;
	}

} // namespace NMLFramework
