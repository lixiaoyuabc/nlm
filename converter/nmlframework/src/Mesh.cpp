/*
Copyright (c) 2012 Nutiteq

This file is part of NML Framework.
*/

#include "Mesh.h"


namespace NMLFramework
{

	//--------------------------------------------------------------------
	int Mesh::getVertexCount() const
	{
		int count = 0;
		for (SubmeshList::const_iterator it = mSubmeshList.begin(); it != mSubmeshList.end(); it++)
		{
			count += it->getVertexCount();
		}
		return count;
	}

	//--------------------------------------------------------------------
	void Mesh::setVertexIds(int id)
	{
		for (SubmeshList::iterator it = mSubmeshList.begin(); it != mSubmeshList.end(); it++)
		{
			it->setVertexIds(id);
		}
	}

	//--------------------------------------------------------------------
	Bounds3 Mesh::calculateBounds(const Matrix4 &transform) const
	{
		// Calculate bounds of each submesh and add them together
		Bounds3 bounds;
		for (SubmeshList::const_iterator it = mSubmeshList.begin(); it != mSubmeshList.end(); it++)
		{
			bounds.add(it->calculateBounds(transform));
		}
		return bounds;
	}

	//--------------------------------------------------------------------
	void Mesh::applyTransformation(const Matrix4 &transform)
	{
		for (SubmeshList::iterator it = mSubmeshList.begin(); it != mSubmeshList.end(); it++)
		{
			it->applyTransformation(transform);
		}
	}

	//--------------------------------------------------------------------
	Mesh Mesh::clone() const
	{
		Mesh mesh(*this);
		return mesh;
	}

	//--------------------------------------------------------------------
	bool Mesh::saveNMLMesh(const std::string &id, NMLPackage::Mesh &nmlMesh) const
	{
		// Create mesh
		nmlMesh.set_id(id);

		// Convert submeshes
		for (SubmeshList::const_iterator it = mSubmeshList.begin(); it != mSubmeshList.end(); it++)
		{
			NMLPackage::Submesh nmlSubmesh;
			it->saveNMLSubmesh(nmlSubmesh);
			*nmlMesh.add_submeshes() = nmlSubmesh;
		}

		// Update bounds info
		*nmlMesh.mutable_bounds() = calculateBounds(Matrix4::IDENTITY).getNMLBounds();
		return true;
	}

	//--------------------------------------------------------------------
	bool Mesh::loadNMLMesh(const NMLPackage::Mesh &nmlMesh)
	{
		mSubmeshList.clear();
		for (int i = 0; i < nmlMesh.submeshes_size(); i++)
		{
			Submesh submesh;
			submesh.loadNMLSubmesh(nmlMesh.submeshes(i));
			mSubmeshList.push_back(submesh);
		}
		return true;
	}

} // namespace NMLFramework
