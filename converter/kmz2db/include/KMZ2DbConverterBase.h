/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#ifndef __KMZ2DB_KMZ2DBCONVERTERBASE_H__
#define __KMZ2DB_KMZ2DBCONVERTERBASE_H__

#include "Prerequisites.h"
#include "Processor.h"
#include "KMZURILoader.h"
#include "DAELoader.h"
#include "KMZModelLODTreeBuilder.h"

#include "COLLADABUURI.h"

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>

#include <pugixml.hpp>


namespace KMZ2Db
{
	using namespace NMLFramework;

	class KMZ2DbConverterBase : public Processor
	{

	public:

		struct Command
		{
			enum Type { ADD, UPDATE, REMOVE };
			Type type;
			std::string globalId; // can be empty for ADD/UPDATE commands - in that case all models from kmzFile are used
			COLLADABU::URI kmzFile; // needed for ADD/UPDATE commands

			explicit Command(Type t) : type(t) { }
		};

		struct Options
		{
			double tileSize;
			bool ignoreHeight;
			bool kmzFilenameAsFolder;
			bool compressLODTreeMeshes;

			Options() : tileSize(1000.0), ignoreHeight(false), kmzFilenameAsFolder(false), compressLODTreeMeshes(false) { }

			void setVariables(const boost::program_options::variables_map &vm)
			{
				tileSize = vm["tile-size"].as<double>();
				ignoreHeight = vm["ignore-height"].as<bool>();
				kmzFilenameAsFolder = vm["kmz-filename-as-folder"].as<bool>();
				compressLODTreeMeshes = vm["compress-lodtree-meshes"].as<bool>();
			}

			static boost::program_options::options_description getDescription()
			{
				namespace po = boost::program_options;
				static const Options opt;
				po::options_description desc("Converter options");
				desc.add_options()
					("tile-size", po::value<double>()->default_value(opt.tileSize), "tile size (meters)")
					("ignore-height", po::value<bool>()->default_value(opt.ignoreHeight), "force all models to ground (ignore height)")
					("kmz-filename-as-folder", po::value<bool>()->default_value(opt.kmzFilenameAsFolder), "replace model folder name with KMZ file name")
					("compress-lodtree-meshes", po::value<bool>()->default_value(opt.compressLODTreeMeshes), "compress LOD tree meshes")
				;
				return desc;
			}
		};

		/** Default constructor */
		KMZ2DbConverterBase(const Options &options, const std::vector<Command>& commands) : mOptions(options), mCommands(commands) { }

		/** Convert KMZ file given by inputUri to multiple NMLPackage classes and store the result in spatialite database. */
		virtual bool convert() = 0;

		/** Options */
		static Options &getDefaultOptions() { return sDefaultOptions; }

	protected:

		typedef std::pair<int, int> TileId;
		typedef KMZModelLODTreeBuilder::ModelId ModelId;
		typedef KMZModelLODTreeBuilder::ModelInfo ModelInfo;
		typedef KMZModelLODTreeBuilder::ModelInfoMap ModelInfoMap;
		typedef boost::shared_ptr<KMZModelLODTreeBuilder> ModelLODTreeBuilderPtr;
		typedef std::map<TileId, ModelLODTreeBuilderPtr> ModelLODTreeBuilderMap;

		const Options mOptions;
		static Options sDefaultOptions;

		std::vector<Command> mCommands;

		Vector3 getTilePos(const TileId &tileId) const;
		TileId getTileId(const Vector3 &mapPos) const;
		void processKML(std::list<ModelInfo> &modelInfos, const pugi::xml_node &root_node, KMZURILoader &uriLoader);
	};

} // namespace KMZ2Db

#endif // __KMZ2DB_KMZ2DBCONVERTERBASE_H__
