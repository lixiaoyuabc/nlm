/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Sqlite.
*/

#ifndef __KMZ2DB_KMZURILOADER_H__
#define __KMZ2DB_KMZURILOADER_H__

#include "Prerequisites.h"

#include "URILoader.h"

#include <zzip/lib.h>

#include <list>


namespace KMZ2Db
{
	using namespace NMLFramework;

	class KMZURILoader : public URILoader
	{

	public:

		KMZURILoader(const std::string &kmzFile, const std::string &rootDir);
		virtual ~KMZURILoader();

		/** Return original kmz file */
		std::string getKmzFile() const { return mKmzFile; }

		/** Return root directory */
		std::string getRootDir() const { return mRootDir; }

		/** List all files with given extension */
		std::list<std::string> getFiles(const std::string &ext);

		/** Return local file name for given input file */
		virtual std::string resolve(const COLLADABU::URI& inputUri);

	private:

		ZZIP_DIR *mKmzDir;
		std::string mKmzFile;
		std::list<std::string> mTempFiles;

		// Disable copy constructor/assignment
		KMZURILoader(const KMZURILoader& pre);
		const KMZURILoader& operator= (const KMZURILoader& pre);
	};

} // namespace KMZ2Db

#endif // __KMZ2DB_KMZURILOADER_H__
