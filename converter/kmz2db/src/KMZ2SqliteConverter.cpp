/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#include "KMZ2SqliteConverter.h"
#include "DAEPreprocessor.h"
#include "Utils.h"
#include "WGS84Utils.h"
#include "TextureExporter.h"

#include <boost/filesystem.hpp>


namespace KMZ2Db
{

	//--------------------------------------------------------------------
	template <typename T>
		bool matchSequence(const std::vector<T> & data, const std::vector<T> & sequence, int position, int offset, int & count, int dim)
	{
		if (position * dim >= (int) sequence.size())
		{
			return true;
		}
		if (offset * dim >= (int) data.size())
		{
			return false;
		}

		count = std::min(count, (int) (data.size() / dim - offset));
		count = std::min(count, (int) (sequence.size() / dim - position));

		for (int i = 0; i < count; i++)
		{
			for (int d = 0; d < dim; d++)
			{
				if (data[d + (i + offset) * dim] != sequence[d + (i + position) * dim])
				{
					count = i;
					return count > 0;
				}
			}
		}
		return count > 0;
	}

	//--------------------------------------------------------------------
	inline bool findSequenceLT(const std::vector<float> & data, const std::vector<float> & sequence, int position, int offset, int & count, std::vector<float> & scale, std::vector<float> & trans, int dim)
	{
		const float EPSILON = 1.0e-5f;

		if (position * dim >= (int) sequence.size())
		{
			return true;
		}
		if (offset * dim > (int) data.size())
		{
			return false;
		}

		count = std::min(count, (int) (data.size() / dim - offset));
		count = std::min(count, (int) (sequence.size() / dim - position));

		if (count == 1)
		{
			for (int d = 0; d < dim; d++)
			{
				scale[d] = 1;
				trans[d] = sequence[d + position * dim] - data[d + offset * dim];
			}
			return true;
		}

		int worstMatchCount = count;
		for (int d = 0; d < dim; d++)
		{
			int matchCount = 0;
			for (int i = 1; i < count; i++)
			{
				int i0 = d + 0 * dim, i1 = d + i * dim;
				double diff = data[i1 + offset * dim] - data[i0 + offset * dim];
				if (diff == 0)
					continue;
				float s = (float) (((double) sequence[i1 + position * dim] - sequence[i0 + position * dim]) / diff);
				float t = (float)  ((double) sequence[i0 + position * dim] - data[i0 + offset * dim] * s);

				for (int j = 0; j < count; j++)
				{
					int j1 = d + j * dim;
					float error = data[j1 + offset * dim] * s + t - sequence[j1 + position * dim];
					if (std::abs(error) > EPSILON)
						break;
					if (j >= matchCount)
					{
						matchCount = j + 1;
						scale[d] = s;
						trans[d] = t;
					}
				}
				if (std::fabs(diff) > EPSILON)
					break;
			}
			if (matchCount < worstMatchCount)
			{
				worstMatchCount = matchCount;
			}
		}
		count = worstMatchCount;
		return count > 0;
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::findSubmeshOpList(const Mesh & srcMesh, const Submesh & submesh, NMLPackage::SubmeshOpList & submeshOpList)
	{
		if (submesh.getVertexCounts().size() != 1)
			return false;

		for (int position = 0; position < submesh.getVertexCount(); )
		{
			NMLPackage::SubmeshOp submeshOp;
			submeshOp.set_count(0);
			for (Mesh::SubmeshList::const_iterator it = srcMesh.getSubmeshList().begin(); it != srcMesh.getSubmeshList().end(); it++)
			{
				const Submesh & srcSubmesh = *it;
				if (srcSubmesh.getType() != submesh.getType())
					continue;

				for (Submesh::IntVector::const_iterator vit = srcSubmesh.getVertexIds().begin(); vit != srcSubmesh.getVertexIds().end(); vit++)
				{
					vit = std::search(vit, srcSubmesh.getVertexIds().end(), submesh.getVertexIds().begin() + position, submesh.getVertexIds().begin() + position + 1);
					if (vit == srcSubmesh.getVertexIds().end())
						break;
					int offset = vit - srcSubmesh.getVertexIds().begin();
					int count = submesh.getVertexCount() - position;
					if (!matchSequence(srcSubmesh.getVertexIds(), submesh.getVertexIds(), position, offset, count, 1))
						continue;
					if (!matchSequence(srcSubmesh.getPositions(), submesh.getPositions(), position, offset, count, 3))
						continue;
					if (!matchSequence(srcSubmesh.getNormals(), submesh.getNormals(), position, offset, count, 3))
						continue;
					if (!matchSequence(srcSubmesh.getColors(), submesh.getColors(), position, offset, count, 4))
						continue;
					std::vector<float> scale(2, 1.0f), trans(2, 0.0f);
					if (!findSequenceLT(srcSubmesh.getUVs(), submesh.getUVs(), position, offset, count, scale, trans, 2))
						continue;
					if (count > submeshOp.count())
					{
						submeshOp.set_submesh_idx(std::distance(srcMesh.getSubmeshList().begin(), it));
						submeshOp.set_offset(offset);
						submeshOp.set_count(count);
						submeshOp.set_tex_u_scale(scale[0]);
						submeshOp.set_tex_v_scale(scale[1]);
						submeshOp.set_tex_u_trans(trans[0]);
						submeshOp.set_tex_v_trans(trans[1]);
						if (position + count == submesh.getVertexCount())
							break;
					}
				}
			}
			int count = submeshOp.count();
			if (count < 3 && position + count < submesh.getVertexCount())
				return false;
			*submeshOpList.add_submesh_ops() = submeshOp;
			position += count;
		}
		submeshOpList.set_type(submesh.getType());
		submeshOpList.set_material_id(submesh.getMaterialId());
		return true;
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::findMeshOp(const Mesh & srcMesh, const Mesh & mesh, NMLPackage::MeshOp & meshOp)
	{
		meshOp.clear_submesh_op_lists();
		for (Mesh::SubmeshList::const_iterator it = mesh.getSubmeshList().begin(); it != mesh.getSubmeshList().end(); it++)
		{
			NMLPackage::SubmeshOpList submeshOpList;
			if (!findSubmeshOpList(srcMesh, *it, submeshOpList))
				return false;
			*meshOp.add_submesh_op_lists() = submeshOpList;
		}
		return true;
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::initializeDb(sqlite3pp::database &db)
	{
		db.execute("PRAGMA page_size=4096"); // this should increase BLOB performance by 2x

		db.execute("CREATE TABLE IF NOT EXISTS Textures(id INTEGER NOT NULL, level INTEGER NOT NULL, nmltexture BLOB NOT NULL, nmltexture_sha1hash BLOB NOT NULL, PRIMARY KEY (id, level))");
		db.execute("CREATE TABLE IF NOT EXISTS Meshes(id INTEGER NOT NULL PRIMARY KEY, nmlmesh BLOB NOT NULL, nmlmesh_sha1hash BLOB NOT NULL)");
		db.execute("CREATE TABLE IF NOT EXISTS Models(id INTEGER NOT NULL PRIMARY KEY, model_key TEXT NOT NULL UNIQUE, nmlmodel BLOB NOT NULL, nmlmodel_sha1hash BLOB NOT NULL)");
		db.execute("CREATE TABLE IF NOT EXISTS ModelLODTrees(id INTEGER NOT NULL PRIMARY KEY ASC, nmlmodellodtree BLOB NOT NULL, nmlmodellodtree_sha1hash BLOB NOT NULL)");
		db.execute("CREATE TABLE IF NOT EXISTS ModelInfo(id INTEGER NOT NULL PRIMARY KEY ASC, modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, global_id TEXT NOT NULL, model_id INTEGER NOT NULL, mappos_x REAL NOT NULL, mappos_y REAL NOT NULL, groundheight REAL NOT NULL, folder TEXT, UNIQUE (modellodtree_id, global_id), UNIQUE (modellodtree_id, model_id))");
		db.execute("CREATE TABLE IF NOT EXISTS MapTiles(id INTEGER NOT NULL PRIMARY KEY ASC, modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, tile_x INTEGER NOT NULL, tile_y INTEGER NOT NULL, tile_size REAL NOT NULL, mappos_x REAL NOT NULL, mappos_y REAL NOT NULL, groundheight REAL NOT NULL, mapbounds_x0 REAL NOT NULL, mapbounds_y0 REAL NOT NULL, mapbounds_x1 REAL NOT NULL, mapbounds_y1 REAL NOT NULL, UNIQUE (tile_x, tile_y, tile_size))");
		db.execute("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeTextures(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, local_id TEXT NOT NULL, texture_id INTEGER NOT NULL, level INTEGER NOT NULL, FOREIGN KEY(texture_id, level) REFERENCES Textures(id, level) ON DELETE CASCADE)");
		db.execute("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeMeshes(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, local_id TEXT NOT NULL, mesh_id INTEGER NOT NULL REFERENCES Meshes(id) ON DELETE CASCADE, nmlmeshop BLOB NULL)");
		db.execute("CREATE TABLE IF NOT EXISTS ModelLODTreeNodeModels(modellodtree_id INTEGER NOT NULL REFERENCES ModelLODTrees(id) ON DELETE CASCADE, node_id INTEGER NOT NULL, model_key TEXT NOT NULL)");

		db.execute("CREATE INDEX IF NOT EXISTS Textures_id ON Textures(id)");
		db.execute("CREATE INDEX IF NOT EXISTS Textures_nmltexture_sha1hash ON Textures(nmltexture_sha1hash)"); // index for fast searching
		db.execute("CREATE INDEX IF NOT EXISTS Meshes_nmlmesh_sha1hash ON Meshes(nmlmesh_sha1hash)");
		db.execute("CREATE INDEX IF NOT EXISTS Models_model_key ON Models(model_key)");
		db.execute("CREATE INDEX IF NOT EXISTS Models_nmlmodel_sha1hash ON Models(nmlmodel_sha1hash)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelLODTrees_nmlmodellodtree_sha1hash ON ModelLODTrees(nmlmodellodtree_sha1hash)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelInfo_modellodtree_id ON ModelInfo(modellodtree_id)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelInfo_modellodtree_id_global_id ON ModelInfo(modellodtree_id, global_id)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelInfo_modellodtree_id_model_id ON ModelInfo(modellodtree_id, model_id)");
		db.execute("CREATE INDEX IF NOT EXISTS MapTiles_modellodtree_id ON MapTiles(modellodtree_id)");
		db.execute("CREATE INDEX IF NOT EXISTS MapTiles_modellodtree_tile ON MapTiles(tile_x, tile_y, tile_size)");
		db.execute("CREATE INDEX IF NOT EXISTS MapTiles_mapbounds_y0 ON MapTiles(mapbounds_y0)");
		db.execute("CREATE INDEX IF NOT EXISTS MapTiles_mapbounds_y1 ON MapTiles(mapbounds_y1)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelLODTreeNodeTextures_modellodtree_id ON ModelLODTreeNodeTextures(modellodtree_id)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelLODTreeNodeMeshes_modellodtree_id ON ModelLODTreeNodeMeshes(modellodtree_id)");
		db.execute("CREATE INDEX IF NOT EXISTS ModelLODTreeNodeModels_modellodtree_id ON ModelLODTreeNodeModels(modellodtree_id)");
		return true;
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::freezeDb(sqlite3pp::database &db)
	{
		db.execute("DROP TABLE Models");
		db.execute("VACUUM");
		return true;
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::convert()
	{
		// Group models into tiles and build list of model references
		mModelLODTreeBuilderMap.clear();
		for (size_t i = 0; i < mCommands.size(); i++)
		{
			const Command &command = mCommands[i];

			if (command.type == Command::ADD || command.type == Command::UPDATE)
			{
				const COLLADABU::URI &inputUri = command.kmzFile;
				reportProgress("convert", "Processing " + inputUri.originalStr());

				KMZURILoader uriLoader(inputUri.toNativePath(), "");
				std::list<std::string> kmlFiles = uriLoader.getFiles(".kml");
				if (kmlFiles.empty())
					reportError("convert", "No KML files inside KMZ!", SEVERITY_WARNING);
				std::list<ModelInfo> modelInfos;
				for (std::list<std::string>::const_iterator it = kmlFiles.begin(); it != kmlFiles.end(); it++)
				{
					std::string kmlFile = uriLoader.resolve(*it);
					reportProgress("convert", "Parsing " + *it);

					pugi::xml_document doc;
					if (!doc.load_file(kmlFile.c_str()))
					{
						reportError("convert", "Could not parse KML file: " + *it, SEVERITY_WARNING);
						continue;
					}
					processKML(modelInfos, doc, uriLoader);
				}
				for (std::list<ModelInfo>::const_iterator it = modelInfos.begin(); it != modelInfos.end(); it++)
				{
					const ModelInfo &modelInfo = *it;
					if (!command.globalId.empty() && modelInfo.globalId != command.globalId)
						continue;

					TileId tileId = getTileId(modelInfo.mapPos);
					ModelLODTreeBuilderPtr builder = getModelLODTreeBuilder(tileId);
					if (command.type == Command::ADD)
					{
						// Do nothing if model has been already added
						if (findModelId(modelInfo.globalId) != -1)
						{
							reportError("convert", "Model already exists, not adding", SEVERITY_WARNING);
							continue;
						}
						builder->add(modelInfo);
					}
					else if (command.type == Command::UPDATE)
					{
						// Split update into remove and add parts - as updated model can move between tiles
						ModelId modelId = findModelId(modelInfo.globalId);
						if (modelId != -1)
						{
							TileId tileId = findTileId(modelInfo.globalId);
							ModelLODTreeBuilderPtr oldBuilder = getModelLODTreeBuilder(tileId);
							oldBuilder->remove(modelId);
						}
						builder->add(modelInfo);
					}
				}
			}
			else if (command.type == Command::REMOVE)
			{
				int modelId = findModelId(command.globalId);
				if (modelId == -1)
				{
					reportError("convert", "Model does not exist, can not remove", SEVERITY_WARNING);
					continue;
				}
				TileId tileId = findTileId(command.globalId);
				ModelLODTreeBuilderPtr builder = getModelLODTreeBuilder(tileId);
				builder->remove(modelId);
			}
		}

		// Create DB lodtree records for each tile
		for (ModelLODTreeBuilderMap::const_iterator it = mModelLODTreeBuilderMap.begin(); it != mModelLODTreeBuilderMap.end(); it++)
		{
			TileId tileId = it->first;
			ModelLODTreeBuilderPtr builder = it->second;

			// Build model tree
			reportProgress("convert", "Building new LOD tree for tile");
			ModelLODTree modelLODTree;
			builder->buildModelLODTree(modelLODTree);

			// Save LOD tree
			reportProgress("convert", "Saving LOD tree");
			long long modelLODTreeId = saveModelLODTree(modelLODTree);

			// Compress LOD tree meshes
			if (mOptions.compressLODTreeMeshes)
			{
				reportProgress("convert", "Compressing LOD tree meshes");
				compressModelLODTreeMeshes(modelLODTreeId);
			}

			// Save model info
			reportProgress("convert", "Saving model info");
			saveModelInfoMap(modelLODTreeId, builder->getAll());

			// Save generated tile
			reportProgress("convert", "Saving map tile");
			Bounds3 localBounds = modelLODTree.getRoot().getModelBounds();
			Vector3 mapPos = getTilePos(tileId);
			long long mapTileId = saveMapTile(modelLODTreeId, tileId, mapPos, localBounds);
		}
		return true;
	}

	//--------------------------------------------------------------------
	KMZ2SqliteConverter::TileId KMZ2SqliteConverter::findTileId(const std::string &globalId) const
	{
		sqlite3pp::query tileQry(mDB, "SELECT mt.tile_x, mt.tile_y FROM MapTiles mt, ModelInfo mi WHERE mi.modellodtree_id=mt.modellodtree_id AND mi.global_id=:global_id AND mt.tile_size=:tile_size");
		tileQry.bind(":global_id", globalId.c_str(), false);
		tileQry.bind(":tile_size", mOptions.tileSize);
		sqlite3pp::query::iterator qit = tileQry.begin();
		if (qit != tileQry.end())
		{
			int x = qit->get<int>(0);
			int y = qit->get<int>(1);
			return std::make_pair(x, y);
		}
		return TileId();
	}

	//--------------------------------------------------------------------
	KMZ2SqliteConverter::ModelId KMZ2SqliteConverter::findModelId(const std::string &globalId) const
	{
		sqlite3pp::query modelQry(mDB, "SELECT mi.model_id FROM MapTiles mt, ModelInfo mi WHERE mi.modellodtree_id=mt.modellodtree_id AND mi.global_id=:global_id AND mt.tile_size=:tile_size");
		modelQry.bind(":global_id", globalId.c_str(), false);
		modelQry.bind(":tile_size", mOptions.tileSize);
		sqlite3pp::query::iterator qit = modelQry.begin();
		if (qit != modelQry.end())
		{
			return qit->get<int>(0);
		}
		return -1;
	}

	//--------------------------------------------------------------------
	KMZ2SqliteConverter::ModelLODTreeBuilderPtr KMZ2SqliteConverter::getModelLODTreeBuilder(const TileId &tileId)
	{
		ModelLODTreeBuilderPtr builder = mModelLODTreeBuilderMap[tileId];
		if (!builder)
		{
			long long modelLODTreeId = -1;
			sqlite3pp::query lodTreeQry(mDB, "SELECT modellodtree_id FROM MapTiles WHERE tile_x=:tile_x AND tile_y=:tile_y AND tile_size=:tile_size");
			lodTreeQry.bind(":tile_x", tileId.first);
			lodTreeQry.bind(":tile_y", tileId.second);
			lodTreeQry.bind(":tile_size", mOptions.tileSize);
			sqlite3pp::query::iterator qit = lodTreeQry.begin();
			if (qit != lodTreeQry.end())
			{
				modelLODTreeId = qit->get<long long>(0);
			}

			ModelLODTree modelLODTree;
			ModelInfoMap modelInfoMap;
			loadModelLODTree(modelLODTreeId, modelLODTree);
			loadModelInfoMap(modelLODTreeId, modelInfoMap);

			Vector3 mapPos = getTilePos(tileId);
			builder.reset(new KMZModelLODTreeBuilder(KMZModelLODTreeBuilder::getDefaultOptions(), *this, modelLODTree, modelInfoMap, mapPos));
			mModelLODTreeBuilderMap[tileId] = builder;
		}
		return builder;
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::loadModelLODTree(long long modelLODTreeId, ModelLODTree &modelLODTree)
	{
		sqlite3pp::query lodTreeQry(mDB, "SELECT nmlmodellodtree, LENGTH(nmlmodellodtree) FROM ModelLODTrees WHERE id=:modellodtree_id");
		lodTreeQry.bind(":modellodtree_id", modelLODTreeId);
		sqlite3pp::query::iterator qit = lodTreeQry.begin();
		if (qit != lodTreeQry.end())
		{
			NMLPackage::ModelLODTree nmlModelLODTree;
			const void * nmlmodellodtree_data = qit->get<const void *>(0);
			int nmlmodellodtree_size = qit->get<int>(1);
			nmlModelLODTree.ParseFromArray(nmlmodellodtree_data, nmlmodellodtree_size);
			ModelLODTree::NodeMap nodeMap;
			modelLODTree.loadNMLModelLODTree(nmlModelLODTree, nodeMap);

			// Load texture bindings
			sqlite3pp::query textureQry(mDB, "SELECT local_id, node_id, texture_id FROM ModelLODTreeNodeTextures WHERE modellodtree_id=:modellodtree_id AND level=0");
			textureQry.bind(":modellodtree_id", modelLODTreeId);
			for (sqlite3pp::query::iterator qit = textureQry.begin(); qit != textureQry.end(); qit++)
			{
				std::string localId = qit->get<std::string>(0);
				int nodeId = qit->get<int>(1);
				long long textureId = qit->get<long long>(2);
				ModelLODTree::Node *node = nodeMap[nodeId];
				if (node == 0)
				{
					reportError("loadModelLODTree", "Inconsistent node mapping", _SEVERITY_ERROR);
					return false;
				}
				node->getTextureBindingMap()[localId] = textureId;
			}

			// Load mesh bindings
			sqlite3pp::query meshQry(mDB, "SELECT local_id, node_id, mesh_id FROM ModelLODTreeNodeMeshes WHERE modellodtree_id=:modellodtree_id");
			meshQry.bind(":modellodtree_id", modelLODTreeId);
			for (sqlite3pp::query::iterator qit = meshQry.begin(); qit != meshQry.end(); qit++)
			{
				std::string localId = qit->get<std::string>(0);
				int nodeId = qit->get<int>(1);
				long long meshId = qit->get<long long>(2);
				ModelLODTree::Node *node = nodeMap[nodeId];
				if (node == 0)
				{
					reportError("loadModelLODTree", "Inconsistent node mapping", _SEVERITY_ERROR);
					return false;
				}
				node->getMeshBindingMap()[localId] = meshId;
			}

			// Load model bindings
			sqlite3pp::query modelQry(mDB, "SELECT node_id, model_key FROM ModelLODTreeNodeModels WHERE modellodtree_id=:modellodtree_id");
			modelQry.bind(":modellodtree_id", modelLODTreeId);
			for (sqlite3pp::query::iterator qit = modelQry.begin(); qit != modelQry.end(); qit++)
			{
				int nodeId = qit->get<int>(0);
				std::string modelKey = qit->get<std::string>(1);
				ModelLODTree::Node *node = nodeMap[nodeId];
				if (node == 0)
				{
					reportError("loadModelLODTree", "Inconsistent node mapping", _SEVERITY_ERROR);
					return false;
				}
				node->setModelId(modelKey);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	long long KMZ2SqliteConverter::saveModelLODTree(const ModelLODTree &modelLODTree)
	{
		TextureExporter textureExporter(TextureExporter::getDefaultOptions());
		NMLPackage::ModelLODTree nmlLODTree;
		ModelLODTree::NodeMap nodeMap;
		modelLODTree.saveNMLModelLODTree(nmlLODTree, nodeMap, textureExporter);
		std::string lodTreeString;
		nmlLODTree.SerializeToString(&lodTreeString);
		std::string lodTreeHash = calculateSHA1(lodTreeString);

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Save LOD tree, by updating existing record
		sqlite3pp::command insertStmt(mDB, "INSERT INTO ModelLODTrees(nmlmodellodtree, nmlmodellodtree_sha1hash) VALUES(:nmlmodellodtree, :nmlmodellodtree_sha1hash)");
		insertStmt.bind(":nmlmodellodtree", lodTreeString.data(), lodTreeString.size(), false);
		insertStmt.bind(":nmlmodellodtree_sha1hash", lodTreeHash.data(), lodTreeHash.size(), false);
		if (insertStmt.execute() != 0)
		{
			reportError("saveModelLODTree", "Unable to insert tree info in database", _SEVERITY_ERROR);
			return -1;
		}
		long long modelLODTreeId = mDB.last_insert_rowid();

		// Save bindings
		for (ModelLODTree::NodeMap::const_iterator nit = nodeMap.begin(); nit != nodeMap.end(); nit++)
		{
			int nodeId = nit->first;
			const ModelLODTree::Node &node = *nit->second;

			// Save texture bindings
			for (ModelLODTree::Node::BindingMap::const_iterator bit = node.getTextureBindingMap().begin(); bit != node.getTextureBindingMap().end(); bit++)
			{
				const std::string &localId = bit->first;
				long long textureId = bit->second;
				sqlite3pp::command bindingStmt(mDB, "INSERT INTO ModelLODTreeNodeTextures(modellodtree_id, node_id, local_id, texture_id, level) VALUES(:modellodtree_id, :node_id, :local_id, :texture_id, :level)");
				bindingStmt.bind(":modellodtree_id", modelLODTreeId);
				bindingStmt.bind(":node_id", nodeId);
				bindingStmt.bind(":local_id", localId.c_str(), false);
				bindingStmt.bind(":texture_id", textureId);
				bindingStmt.bind(":level", 0);
				if (bindingStmt.execute() != 0)
				{
					reportError("saveModelLODTree", "Unable to insert texture binding into database", _SEVERITY_ERROR);
					return -1;
				}
			}

			// Save mesh bindings
			for (ModelLODTree::Node::BindingMap::const_iterator bit = node.getMeshBindingMap().begin(); bit != node.getMeshBindingMap().end(); bit++)
			{
				const std::string &localId = bit->first;
				long long meshId = bit->second;
				sqlite3pp::command bindingStmt(mDB, "INSERT INTO ModelLODTreeNodeMeshes(modellodtree_id, node_id, local_id, mesh_id) VALUES(:modellodtree_id, :node_id, :local_id, :mesh_id)");
				bindingStmt.bind(":modellodtree_id", modelLODTreeId);
				bindingStmt.bind(":node_id", nodeId);
				bindingStmt.bind(":local_id", localId.c_str(), false);
				bindingStmt.bind(":mesh_id", meshId);
				if (bindingStmt.execute() != 0)
				{
					reportError("saveModelLODTree", "Unable to insert mesh binding into database", _SEVERITY_ERROR);
					return -1;
				}
			}

			// Save model binding
			const std::string &modelKey = nit->second->getModelId();
			sqlite3pp::command bindingStmt(mDB, "INSERT INTO ModelLODTreeNodeModels(modellodtree_id, node_id, model_key) VALUES(:modellodtree_id, :node_id, :model_key)");
			bindingStmt.bind(":modellodtree_id", modelLODTreeId);
			bindingStmt.bind(":node_id", nodeId);
			bindingStmt.bind(":model_key", modelKey.c_str(), false);
			if (bindingStmt.execute() != 0)
			{
				reportError("saveModelLODTree", "Unable to insert model binding into database", _SEVERITY_ERROR);
				return -1;
			}
		}

		// Success, commit changes to db
		xct.commit();
		return modelLODTreeId;
	}

	//--------------------------------------------------------------------
	void KMZ2SqliteConverter::compressModelLODTreeMeshes(long long modelLODTreeId)
	{
		typedef std::pair<long long, Mesh> IdMeshPair;

		std::list<IdMeshPair> finalMeshes;
		sqlite3pp::query meshQry(mDB, "SELECT m.id, m.nmlmesh, LENGTH(m.nmlmesh) AS len, mb.node_id, mb.local_id FROM Meshes m, ModelLODTreeNodeMeshes mb WHERE m.id=mb.mesh_id AND mb.modellodtree_id=:modellodtree_id AND mb.nmlmeshop IS NULL ORDER BY node_id ASC, len DESC");
		meshQry.bind(":modellodtree_id", modelLODTreeId);
		for (sqlite3pp::query::iterator qit = meshQry.begin(); qit != meshQry.end(); qit++)
		{
			long long meshId = qit->get<long long>(0);
			const void * nmlmesh_data = qit->get<const void *>(1);
			int nmlmesh_size = qit->get<int>(2);
			int nodeId = qit->get<int>(3);
			std::string localId = qit->get<std::string>(4);

			NMLPackage::Mesh nmlMesh;
			nmlMesh.ParseFromArray(nmlmesh_data, nmlmesh_size);
			Mesh mesh;
			mesh.loadNMLMesh(nmlMesh);

			NMLPackage::MeshOp meshOp;
			long long finalMeshId = -1;
			for (std::list<IdMeshPair>::const_iterator it = finalMeshes.begin(); it != finalMeshes.end(); it++)
			{
				if (findMeshOp(it->second, mesh, meshOp))
				{
					meshOp.set_id(nmlMesh.id());
					*meshOp.mutable_bounds() = nmlMesh.bounds();
					finalMeshId = it->first;
					break;
				}
			}

			if (finalMeshId != -1)
			{
				std::string meshOpString;
				meshOp.SerializeToString(&meshOpString);

				sqlite3pp::command meshStmt(mDB, "UPDATE ModelLODTreeNodeMeshes SET nmlmeshop=:nmlmeshop, mesh_id=:final_mesh_id WHERE mesh_id=:mesh_id AND modellodtree_id=:modellodtree_id AND node_id=:node_id AND local_id=:local_id AND nmlmeshop IS NULL");
				meshStmt.bind(":nmlmeshop", meshOpString.data(), meshOpString.size(), false);
				meshStmt.bind(":final_mesh_id", finalMeshId);
				meshStmt.bind(":mesh_id", meshId);
				meshStmt.bind(":modellodtree_id", modelLODTreeId);
				meshStmt.bind(":node_id", nodeId);
				meshStmt.bind(":local_id", localId.c_str(), false);
				meshStmt.execute();

				sqlite3pp::query optQry(mDB, "SELECT COUNT(*) FROM ModelLODTreeNodeMeshes WHERE mesh_id=:mesh_id");
				optQry.bind(":mesh_id", meshId);
				if (optQry.begin()->get<int>(0) == 0)
				{
					sqlite3pp::command optStmt(mDB, "DELETE FROM Meshes WHERE id=:mesh_id");
					optStmt.bind(":mesh_id", meshId);
					optStmt.execute();
				}
			}
			else
			{
				finalMeshes.push_back(std::make_pair(meshId, mesh));
			}
		}
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::loadModelInfoMap(long long modelLODTreeId, ModelInfoMap &modelInfoMap)
	{
		sqlite3pp::query modelInfoQry(mDB, "SELECT id, global_id, model_id, mappos_x, mappos_y, groundheight, folder FROM ModelInfo WHERE modellodtree_id=:modellodtree_id");
		modelInfoQry.bind(":modellodtree_id", modelLODTreeId);
		for (sqlite3pp::query::iterator qit = modelInfoQry.begin(); qit != modelInfoQry.end(); qit++)
		{
			long long id = qit->get<long long>(0);
			std::string globalId = qit->get<std::string>(1);
			int modelId = qit->get<int>(2);
			Vector3 mapPos(qit->get<double>(3), qit->get<double>(4), qit->get<double>(5));
			std::string folder = qit->get<std::string>(6);
			ModelInfo modelInfo;
			modelInfo.sourceId = id;
			modelInfo.globalId = globalId;
			modelInfo.mapPos = mapPos;
			modelInfo.folder = folder;
			modelInfoMap[modelId] = modelInfo;
		}
		return true;
	}

	//--------------------------------------------------------------------
	void KMZ2SqliteConverter::saveModelInfoMap(long long modelLODTreeId, const ModelInfoMap &modelInfoMap)
	{
		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Delete existing duplicate model info
		boost::lock_guard<boost::mutex> lock(mDBMutex);
		sqlite3pp::command infoDeleteStmt(mDB, "DELETE FROM ModelInfo WHERE modellodtree_id=:modellodtree_id");
		infoDeleteStmt.bind(":modellodtree_id", modelLODTreeId);
		if (infoDeleteStmt.execute() != 0)
		{
			reportError("saveModelInfoMap", "Unable to delete old model info from database", _SEVERITY_ERROR);
			return;
		}

		// Save model info
		for (ModelInfoMap::const_iterator it = modelInfoMap.begin(); it != modelInfoMap.end(); it++)
		{
			int modelId = it->first;
			const ModelInfo &modelInfo = it->second;

			// Insert new info record
			std::string folder = mOptions.kmzFilenameAsFolder ? boost::filesystem::path(modelInfo.kmzFile).stem().string() : modelInfo.folder;
			sqlite3pp::command infoInsertStmt(mDB, "INSERT INTO ModelInfo(modellodtree_id, global_id, model_id, mappos_x, mappos_y, groundheight, folder) VALUES(:modellodtree_id, :global_id, :model_id, :mappos_x, :mappos_y, :groundheight, :folder)");
			infoInsertStmt.bind(":modellodtree_id", modelLODTreeId);
			infoInsertStmt.bind(":global_id", modelInfo.globalId.c_str(), false);
			infoInsertStmt.bind(":model_id", modelId);
			infoInsertStmt.bind(":folder", folder.c_str(), false);
			infoInsertStmt.bind(":mappos_x", modelInfo.mapPos.x);
			infoInsertStmt.bind(":mappos_y", modelInfo.mapPos.y);
			infoInsertStmt.bind(":groundheight", modelInfo.mapPos.z);
			if (infoInsertStmt.execute() != 0)
			{
				reportError("saveModelInfoMap", "Unable to insert model info into database", _SEVERITY_ERROR);
				return;
			}
		}

		// Success, commit changes to db
		xct.commit();
	}

	//--------------------------------------------------------------------
	long long KMZ2SqliteConverter::saveMapTile(long long modelLODTreeId, const TileId &tileId, const Vector3 &mapPos, const Bounds3 &localBounds)
	{
		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Delete existing duplicate tile (based on tile id)
		boost::lock_guard<boost::mutex> lock(mDBMutex);
		sqlite3pp::command deleteStmt(mDB, "DELETE FROM MapTiles WHERE tile_x=:tile_x AND tile_y=:tile_y AND tile_size=:tile_size");
		deleteStmt.bind(":tile_x", tileId.first);
		deleteStmt.bind(":tile_y", tileId.second);
		deleteStmt.bind(":tile_size", mOptions.tileSize);
		if (deleteStmt.execute() != 0)
		{
			reportError("saveMapTile", "Unable to delete old map tile from database", _SEVERITY_ERROR);
			return -1;
		}
		if (localBounds.empty())
			return -1;

		// Insert new tile
		Matrix4 globalTransform = internalWebMercatorTransformation(mapPos);
		Bounds3 mapBounds(globalTransform * localBounds.min, globalTransform * localBounds.max);
		sqlite3pp::command insertStmt(mDB, "INSERT INTO MapTiles(modellodtree_id, tile_x, tile_y, tile_size, mappos_x, mappos_y, groundheight, mapbounds_x0, mapbounds_y0, mapbounds_x1, mapbounds_y1) VALUES(:modellodtree_id, :tile_x, :tile_y, :tile_size, :mappos_x, :mappos_y, :groundheight, :mapbounds_x0, :mapbounds_y0, :mapbounds_x1, :mapbounds_y1)");
		insertStmt.bind(":modellodtree_id", modelLODTreeId);
		insertStmt.bind(":tile_x", tileId.first);
		insertStmt.bind(":tile_y", tileId.second);
		insertStmt.bind(":tile_size", mOptions.tileSize);
		insertStmt.bind(":mappos_x", mapPos.x);
		insertStmt.bind(":mappos_y", mapPos.y);
		insertStmt.bind(":groundheight", mapPos.z);
		insertStmt.bind(":mapbounds_x0", mapBounds.min.x);
		insertStmt.bind(":mapbounds_y0", mapBounds.min.y);
		insertStmt.bind(":mapbounds_x1", mapBounds.max.x);
		insertStmt.bind(":mapbounds_y1", mapBounds.max.y);
		if (insertStmt.execute() != 0)
		{
			reportError("saveMapTile", "Unable to insert map tile into database", _SEVERITY_ERROR);
			return -1;
		}
		long long mapTileId = mDB.last_insert_rowid();

		// Success, commit changes to db
		xct.commit();
		return mapTileId;
	}

	//--------------------------------------------------------------------
	bool KMZ2SqliteConverter::loadModel(const std::string &modelKey, Model &model)
	{
		sqlite3pp::query modelQry(mDB, "SELECT id, nmlmodel, LENGTH(nmlmodel) FROM Models WHERE model_key=:model_key");
		modelQry.bind(":model_key", modelKey.c_str(), false);
		sqlite3pp::query::iterator qit = modelQry.begin();
		if (qit != modelQry.end())
		{
			const void * nmlmodel_data = qit->get<const void *>(1);
			int nmlmodel_size = qit->get<int>(2);
			NMLPackage::Model nmlModel;
			nmlModel.ParseFromArray(nmlmodel_data, nmlmodel_size);
			return model.loadNMLModel(nmlModel);
		}
		return false;
	}

	//--------------------------------------------------------------------
	long long KMZ2SqliteConverter::saveModel(const std::string &modelKey, const Model &model)
	{
		TextureExporter::Options options;
		options.generateMipMaps = false;
		options.RGBFormat = TextureExporter::Options::PNG;
		options.RGBAFormat = TextureExporter::Options::PNG;
		TextureExporter textureExporter(options);
		NMLPackage::Model nmlModel;
		model.saveNMLModel(modelKey, nmlModel, textureExporter);

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		sqlite3pp::command deleteStmt(mDB, "DELETE FROM Models WHERE model_key=:model_key");
		deleteStmt.bind(":model_key", modelKey.c_str(), false);
		if (deleteStmt.execute() != 0)
		{
			reportError("saveModel", "Unable to delete model from database", _SEVERITY_ERROR);
		}

		std::string modelString;
		nmlModel.SerializeToString(&modelString);
		std::string modelHash = calculateSHA1(modelString);

		sqlite3pp::command insertStmt(mDB, "INSERT INTO Models(model_key, nmlmodel, nmlmodel_sha1hash) VALUES(:model_key, :nmlmodel, :nmlmodel_sha1hash)");
		insertStmt.bind(":model_key", modelKey.c_str(), false);
		insertStmt.bind(":nmlmodel", modelString.data(), modelString.size(), false);
		insertStmt.bind(":nmlmodel_sha1hash", modelHash.data(), modelHash.size(), false);
		if (insertStmt.execute() != 0)
		{
			reportError("saveModel", "Unable to insert new model into database", _SEVERITY_ERROR);
			return -1;
		}
		long long id = mDB.last_insert_rowid();

		// Success, commit changes to db
		xct.commit();
		return id;
	}

	//--------------------------------------------------------------------
	long long KMZ2SqliteConverter::saveMesh(const std::string &meshId, const Mesh &mesh)
	{
		NMLPackage::Mesh nmlMesh;
		mesh.saveNMLMesh(meshId, nmlMesh);
		std::string meshString;
		nmlMesh.SerializeToString(&meshString);
		std::string meshHash = calculateSHA1(meshString);

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Save NML mesh if not saved already
		long long id = -1;
		sqlite3pp::query meshQry(mDB, "SELECT id FROM Meshes WHERE nmlmesh_sha1hash=:nmlmesh_sha1hash AND nmlmesh=:nmlmesh");
		meshQry.bind(":nmlmesh", meshString.data(), meshString.size(), false);
		meshQry.bind(":nmlmesh_sha1hash", meshHash.data(), meshHash.size(), false);
		sqlite3pp::query::iterator qit = meshQry.begin();
		if (qit != meshQry.end())
		{
			id = qit->get<long long>(0);
		}
		else
		{
			sqlite3pp::command meshStmt(mDB, "INSERT INTO Meshes(nmlmesh, nmlmesh_sha1hash) VALUES(:nmlmesh, :nmlmesh_sha1hash)");
			meshStmt.bind(":nmlmesh", meshString.data(), meshString.size(), false);
			meshStmt.bind(":nmlmesh_sha1hash", meshHash.data(), meshHash.size(), false);
			if (meshStmt.execute() != 0)
			{
				reportError("saveMesh", "Unable to insert mesh info into database", _SEVERITY_ERROR);
				return -1;
			}
			id = mDB.last_insert_rowid();
		}

		// Success, commit changes to db
		xct.commit();
		return id;
	}

	//--------------------------------------------------------------------
	long long KMZ2SqliteConverter::saveTexture(const std::string &textureId, const Texture &texture)
	{
		TextureExporter textureExporter(TextureExporter::getDefaultOptions());
		NMLPackage::Texture nmlTexture;
		texture.saveNMLTexture(textureId, nmlTexture, textureExporter);
		std::string textureString;
		nmlTexture.SerializeToString(&textureString);
		std::string textureHash = calculateSHA1(textureString);

		// Start new transaction
		sqlite3pp::transaction xct(mDB);

		// Save NML texture if not saved already
		long long id = -1;
		sqlite3pp::query textureQry(mDB, "SELECT id FROM Textures WHERE nmltexture_sha1hash=:nmltexture_sha1hash AND nmltexture=:nmltexture AND level=0");
		textureQry.bind(":nmltexture", textureString.data(), textureString.size(), false);
		textureQry.bind(":nmltexture_sha1hash", textureHash.data(), textureHash.size(), false);
		sqlite3pp::query::iterator qit = textureQry.begin();
		if (qit != textureQry.end())
		{
			id = qit->get<long long>(0);
		}
		else
		{
			boost::lock_guard<boost::mutex> lock(mDBMutex);
			id = sqlite3pp::query(mDB, "SELECT MAX(id) FROM Textures").begin()->get<long long>(0) + 1;
			sqlite3pp::command textureStmt(mDB, "INSERT INTO Textures(id, level, nmltexture, nmltexture_sha1hash) VALUES(:id, 0, :nmltexture, :nmltexture_sha1hash)");
			textureStmt.bind(":id", id);
			textureStmt.bind(":nmltexture", textureString.data(), textureString.size(), false);
			textureStmt.bind(":nmltexture_sha1hash", textureHash.data(), textureHash.size(), false);
			if (textureStmt.execute() != 0)
			{
				reportError("saveTexture", "Unable to insert texture info into database", _SEVERITY_ERROR);
				return -1;
			}
		}

		// Success, commit changes to db
		xct.commit();
		return id;
	}

} // namespace KMZ2Db
