/*
Copyright (c) 2012 Nutiteq

This file is part of KMZ2Db.
*/

#include "KMZURILoader.h"

#include <boost/filesystem.hpp>


namespace KMZ2Db
{

	//--------------------------------------------------------------------
	KMZURILoader::KMZURILoader(const std::string &kmzFile, const std::string &rootDir)
		: URILoader(rootDir), mKmzDir(0), mKmzFile(kmzFile), mTempFiles()
	{
		if (!boost::filesystem::is_directory(kmzFile))
		{
			mKmzDir = zzip_dir_open(kmzFile.c_str(), 0);
			if (!mKmzDir)
				throw URIException("Could not open KMZ file");
		}
	}

	//--------------------------------------------------------------------
	KMZURILoader::~KMZURILoader()
	{
		if (mKmzDir != 0)
			zzip_dir_close(mKmzDir);
		for (std::list<std::string>::const_iterator it = mTempFiles.begin(); it != mTempFiles.end(); it++)
		{
			boost::filesystem::remove(*it);
		}
	}

	//--------------------------------------------------------------------
	std::string KMZURILoader::resolve(const COLLADABU::URI& inputUri)
	{
		if (!inputUri.getScheme().empty())
			throw URIException("Unsupported protocol: " + inputUri.getScheme());

		if (inputUri.getPath().substr(0, 1) == "/")
			return inputUri.toNativePath();

		if (mKmzDir == 0)
		{
			boost::filesystem::path p(mKmzFile);
			p /= mRootDir;
			p /= inputUri.getPath();
			return p.generic_string();
		}

		std::string uriPath = mRootDir + inputUri.getPath();
		std::vector<char> uriPathBuf(uriPath.size() + 1);
		std::copy(uriPath.begin(), uriPath.end(), uriPathBuf.begin());
		COLLADABU::URI::normalizeURIPath(&uriPathBuf[0]);
		uriPath = &uriPathBuf[0];
		std::string inputFileName = COLLADABU::URI::uriDecode(uriPath);
		
		ZZIP_FILE *zipFile = zzip_file_open(mKmzDir, inputFileName.c_str(), 0);
		if (!zipFile)
			throw URIException("Unable to find file in KMZ: " + inputFileName);

		std::string tempFileName;
		try
		{
			boost::filesystem::path tempFilePath = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path("KMZ-%%%%-%%%%-%%%%-%%%%." + inputUri.getPathExtension());
			tempFileName = tempFilePath.string();
			mTempFiles.push_back(tempFileName);
			FILE *tempFile = fopen(tempFileName.c_str(), "wb");
			if (!tempFile)
				throw URIException("Unable to create temporary file");

			try
			{
				while (true)
				{
					char buf[1024];
					zzip_ssize_t len = zzip_file_read(zipFile, buf, sizeof(buf));
					if (len < 0)
						throw URIException("Unable to read KMZ archive");
					if (fwrite(buf, 1, len, tempFile) != (size_t) len)
						throw URIException("Unable to write to temporary file");
					if ((size_t) len < sizeof(buf))
						break;
				}
				fclose(tempFile);
			}
			catch (...)
			{
				fclose(tempFile);
				throw;
			}
			zzip_file_close(zipFile);
		}
		catch (...)
		{
			zzip_file_close(zipFile);
			throw;
		}
		return tempFileName;
	}

	//--------------------------------------------------------------------
	std::list<std::string> KMZURILoader::getFiles(const std::string &ext)
	{
		std::list<std::string> filenames;
		if (!mKmzDir)
		{
			boost::filesystem::path p(mKmzFile);
			p /= mRootDir;
			for (boost::filesystem::directory_iterator dir_iter(p); dir_iter != boost::filesystem::directory_iterator(); ++dir_iter)
			{
				if ((*dir_iter).path().extension().string() == ext)
				{
					filenames.push_back((*dir_iter).path().filename().string());
				}
			}
 		}
		else
		{
			ZZIP_DIRENT dirent;
			while (zzip_dir_read(mKmzDir, &dirent))
			{
				std::string filename = dirent.d_name;
				if (ext.empty() || (filename.size() > ext.size() && filename.substr(filename.size() - ext.size()) == ext))
				{
					filenames.push_back(filename);
				}
			}
		}
		return filenames;
	}

} // namespace KMZ2Db
