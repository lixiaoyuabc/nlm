set(name protobuf)
project(${name})

set(libprotobuf_include_dirs
	${CMAKE_CURRENT_SOURCE_DIR}/include
	${CMAKE_CURRENT_SOURCE_DIR}/src
)

set(libprotobuf_include_dirs ${libprotobuf_include_dirs} PARENT_SCOPE)  # adding include dirs to a parent scope

set(SRC
src/google/protobuf/descriptor.cc
src/google/protobuf/descriptor_database.cc
src/google/protobuf/descriptor.pb.cc
src/google/protobuf/dynamic_message.cc
src/google/protobuf/extension_set.cc
src/google/protobuf/extension_set_heavy.cc
src/google/protobuf/generated_message_reflection.cc
src/google/protobuf/generated_message_util.cc
src/google/protobuf/message.cc
src/google/protobuf/message_lite.cc
src/google/protobuf/reflection_ops.cc
src/google/protobuf/repeated_field.cc
src/google/protobuf/service.cc
src/google/protobuf/text_format.cc
src/google/protobuf/unknown_field_set.cc
src/google/protobuf/wire_format.cc
src/google/protobuf/wire_format_lite.cc

src/google/protobuf/io/coded_stream.cc
src/google/protobuf/io/zero_copy_stream_impl.cc
src/google/protobuf/io/tokenizer.cc
src/google/protobuf/io/zero_copy_stream_impl_lite.cc
src/google/protobuf/io/gzip_stream.cc
src/google/protobuf/io/printer.cc
src/google/protobuf/io/zero_copy_stream.cc

src/google/protobuf/stubs/common.cc
src/google/protobuf/stubs/once.cc
src/google/protobuf/stubs/strutil.cc
src/google/protobuf/stubs/substitute.cc
)

set(libprotobuf_libs)
if (WIN32)
else ()
	list(APPEND libprotobuf_libs pthread)
endif ()

include_directories(
	${libprotobuf_include_dirs}
)
link_directories(${LIBRARY_OUTPUT_PATH})

add_library(${name} ${SRC})
target_link_libraries(${name} ${libprotobuf_libs})
set(CMAKE_REQUIRED_LIBRARIES "${name};${CMAKE_REQUIRED_LIBRARIES}" PARENT_SCOPE)
