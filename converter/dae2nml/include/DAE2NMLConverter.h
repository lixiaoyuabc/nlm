/*
Copyright (c) 2012 Nutiteq

This file is part of DAE2NML.
*/

#ifndef __DAE2NML_DAE2NMLCONVERTER_H__
#define __DAE2NML_DAE2NMLCONVERTER_H__

#include "Prerequisites.h"
#include "Processor.h"
#include "Model.h"

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>


namespace DAE2NML
{
	using namespace NMLFramework;

	class DAE2NMLConverter : public Processor
	{

	public:

		struct Options
		{
			int lightingModel;
			int maxMergedColorVertices;
			int maxSingleTextureSize;
			int maxAtlasTextureSize;
			bool squareTextureAtlas;

			Options() : lightingModel(1), maxMergedColorVertices(4096), maxSingleTextureSize(256), maxAtlasTextureSize(1024), squareTextureAtlas(false) { }

			void setVariables(const boost::program_options::variables_map &vm)
			{
				lightingModel = vm["lighting-model"].as<int>();
				maxMergedColorVertices = vm["max-merged-color-vertices"].as<int>();
				maxSingleTextureSize = Texture::roundToPOT(vm["max-single-texture-size"].as<int>());
				maxAtlasTextureSize = vm["max-atlas-texture-size"].as<int>() > 0 ? Texture::roundToPOT(vm["max-atlas-texture-size"].as<int>()) : 0;
				squareTextureAtlas = vm["square-texture-atlas"].as<bool>();
			}

			static boost::program_options::options_description getDescription()
			{
				namespace po = boost::program_options;
				static const Options opt;
				po::options_description desc("Converter options");
				desc.add_options()
					("lighting-model", po::value<int>()->default_value(opt.lightingModel), "lighting model (0=none, 1=directional sun)")
					("max-merged-color-vertices", po::value<int>()->default_value(opt.maxMergedColorVertices), "maximum merged color vertices")
					("max-single-texture-size", po::value<int>()->default_value(opt.maxSingleTextureSize), "maximum texture size")
					("max-atlas-texture-size", po::value<int>()->default_value(opt.maxAtlasTextureSize), "maximum texture atlas size (0=disable atlas generation)")
					("square-texture-atlas", po::value<bool>()->default_value(opt.squareTextureAtlas), "generate square texture atlases")
				;
				return desc;
			}
		};

		DAE2NMLConverter(const Options &options, const Model &inputModel, NMLPackage::Model &outputModel) : mOptions(options), mInputModel(inputModel), mOutputModel(outputModel) { }

		/** Convert */
		bool convert();

		/** Options */
		static Options &getDefaultOptions() { return sDefaultOptions; }

	private:

		const Model &mInputModel;
		NMLPackage::Model &mOutputModel;

		const Options mOptions;
		static Options sDefaultOptions;
	};

} // namespace DAE2NML

#endif // __DAE2NML_DAE2NMLCONVERTER_H__
