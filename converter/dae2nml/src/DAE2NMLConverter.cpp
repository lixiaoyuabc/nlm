/*
Copyright (c) 2012 Nutiteq

This file is part of DAE2NML.
*/

#include "DAE2NMLConverter.h"
#include "TextureExporter.h"

namespace DAE2NML
{

	//--------------------------------------------------------------------
	DAE2NMLConverter::Options DAE2NMLConverter::sDefaultOptions;

	//--------------------------------------------------------------------
	bool DAE2NMLConverter::convert()
	{
		Model model = mInputModel;

		// Apply lighting
		if (mOptions.lightingModel != 0)
		{
			reportProgress("convert", "Generating lighting for model");
			model.applyLightingModel(BasicLightingModel(mOptions.lightingModel), Matrix4::IDENTITY);
		}

		// Optimize combined model
		reportProgress("convert", "Optimizing model");
		model.stripPointsLines();
		model.mergeMaterials();
		model.mergeSubmeshes();
		model.cleanup();
		model.generateVertexColors(mOptions.maxMergedColorVertices);
		model.mergeMaterials();
		model.mergeSubmeshes();
		model.cleanup();
		model.resizeTexturesToPOT(mOptions.maxSingleTextureSize);
		if (mOptions.maxAtlasTextureSize > 0)
		{
			model.generateTextureAtlas(mOptions.maxSingleTextureSize, mOptions.maxAtlasTextureSize, mOptions.squareTextureAtlas, 1048576, 4096);
		}
		model.cleanup();
		model.mergeMaterials();
		model.mergeSubmeshes();
		model.cleanup();
		model.canonizeNames();

		// Convert to NML model and store bounds
		reportProgress("convert", "Building NML model");
		TextureExporter textureExporter(TextureExporter::getDefaultOptions());
		model.saveNMLModel("", mOutputModel, textureExporter);

		return true;
	}

} // namespace DAE2NML
