/*
Copyright (c) 2012 Nutiteq

This file is part of geojson2db.
*/

#include "GeoJsonModelLODTreeBuilder.h"
#include "Triangulator.h"
#include "TextureExporter.h"
#include "WGS84Utils.h"
#include "Utils.h"

#include <boost/filesystem.hpp>

namespace GeoJson2Db
{

	//--------------------------------------------------------------------
	GeoJsonModelLODTreeBuilder::Options GeoJsonModelLODTreeBuilder::sOptions;

	//--------------------------------------------------------------------
	void GeoJsonModelLODTreeBuilder::buildNMLModelLODTree(NMLPackage::ModelLODTree &nmlLODTree, Vector3 &mapPos, Bounds3 &localBounds)
	{
		// Create model tree
		reportProgress("buildModelLODTree", "Building model LOD tree index");
		std::list<ModelData> modelList;
		SpatialModelTree::ObjectBoundsPairList spatialObjList;
		for (ModelMap::const_iterator it = mModelMap.begin(); it != mModelMap.end(); it++)
		{
			ModelId modelId = it->first;
			const ModelInfo &modelInfo = it->second;
			Vector3 center(0, 0, 0);
			Bounds3 bounds;
			for (size_t i = 0; i < modelInfo.contour.size(); i++)
			{
				Vector3 p0(modelInfo.contour[i]);
				Vector3 p1(p0.x, p0.y, modelInfo.height);
				center += p0 * (1.0 / modelInfo.contour.size());
				bounds.add(p0);
				bounds.add(p1);
			}
			ModelData modelData;
			modelData.modelId = modelId;
			modelList.push_back(modelData);
			spatialObjList.push_back(std::make_pair(&modelList.back(), bounds));
		}

		SpatialModelTree spatialTree;
		spatialTree.create(spatialObjList, getOptions().finalNodeSize, getOptions().initialNodeSize);

		// Start processing from root
		reportProgress("buildNMLModelLODTree", "Building model LOD tree content");
		SpatialModelTree::Node *spatialTreeRoot = spatialTree.getRoot();
		localBounds = Bounds3();
		if (spatialTreeRoot != 0)
		{
			Vector3 wgs84Location = webMercatorToWgs84(spatialTreeRoot->getNodeBounds().center());
			wgs84Location.z = spatialTreeRoot->getNodeBounds().min.z; // force minimum height
			mapPos = wgs84ToWebMercator(wgs84Location); // this enforces position to be in valid range
			createNode(spatialTreeRoot, nmlLODTree, localBounds, wgs84Location);
		}
	}

	//--------------------------------------------------------------------
	int GeoJsonModelLODTreeBuilder::createNode(SpatialModelTree::Node *spatialTreeNode, NMLPackage::ModelLODTree &nmlLODTree, Bounds3 &modelTreeBounds, const Vector3 &wgs84Location)
	{
		Matrix4 globalTransform = internalWgs84Transformation(wgs84Location);

		// Create tree node
		int nmlNodeIdx = nmlLODTree.nodes().size();
		NMLPackage::ModelLODTreeNode & nmlLODTreeNode = *nmlLODTree.add_nodes();
		nmlLODTreeNode.set_id(nmlNodeIdx);

		// Process children first
		for (int n = 0; n < SpatialModelTree::Node::CHILDREN; n++)
		{
			if (spatialTreeNode->getChild(n) != 0)
				nmlLODTreeNode.add_children_ids(createNode(spatialTreeNode->getChild(n), nmlLODTree, modelTreeBounds, wgs84Location));
		}

		// Load models under this node
		for (SpatialModelTree::ObjectList::iterator it = spatialTreeNode->getObjects().begin(); it != spatialTreeNode->getObjects().end(); it++)
		{
			loadModelData(**it, wgs84Location);
		}

		// Add models from child nodes
		for (int n = 0; n < SpatialModelTree::Node::CHILDREN; n++)
		{
			if (spatialTreeNode->getChild(n) != 0)
				spatialTreeNode->addObjects(*spatialTreeNode->getChild(n));
		}

		// Concatenate models, keep only single model with many meshes
		if (spatialTreeNode->getObjects().size() > 1)
		{
			ModelData &modelData = *spatialTreeNode->getObjects().front();
			modelData.modelId = -1;
			modelData.id.clear();
			modelData.folder.clear();
			for (SpatialModelTree::ObjectList::iterator it = spatialTreeNode->getObjects().begin(); ++it != spatialTreeNode->getObjects().end(); )
			{
				concatenateModelData(modelData, *(*it));
			}
			spatialTreeNode->getObjects().resize(1); // remove concatenated models
		}

		// Optimize and combine models. Do here only 'progressive' optimizations. Full optimizations are done after combining.
		reportProgress("createNode", "Optimizing and combining models");
		Model combinedModel;
		for (SpatialModelTree::ObjectList::const_iterator it = spatialTreeNode->getObjects().begin(); it != spatialTreeNode->getObjects().end(); it++)
		{
			ModelData &modelData = **it;
			Model model = createSimplifiedModel(modelData, globalTransform, spatialTreeNode->getNodeBounds().size());
			model.flatten(getOptions().maxSubmeshVertices, 256);
			model.optimizeForBoundsSize(globalTransform, spatialTreeNode->getNodeBounds().size(), getOptions().minSubmeshSize, getOptions().targetScreenSize, 256);
			model.mergeMaterials();
			model.cleanup();
			combinedModel.concat(model);
		}

		// Apply lighting
		if (getOptions().lightingModel != 0)
		{
			reportProgress("createNode", "Generating lighting for combined model");
			combinedModel.applyLightingModel(BasicLightingModel(getOptions().lightingModel), globalTransform);
		}

		// Optimize combined model
		reportProgress("createNode", "Optimizing combined model");
		combinedModel.stripPointsLines();
		combinedModel.flatten(getOptions().maxExtraFlattenVertices, getOptions().maxSubmeshVertices);
		combinedModel.mergeMaterials();
		combinedModel.mergeSubmeshes();
		combinedModel.cleanup();
		combinedModel.generateVertexColors(getOptions().maxMergedColorVertices);
		combinedModel.mergeMaterials();
		combinedModel.mergeSubmeshes();
		combinedModel.cleanup();
		//removeDuplicateTriangles(combinedModel);
		combinedModel.splitSubmeshes(getOptions().maxSubmeshVertices);
		combinedModel.canonizeNames();

		// Convert to NML model and store bounds
		reportProgress("createNode", "Building NML model");
		TextureExporter textureExporter(TextureExporter::getDefaultOptions());
		combinedModel.saveNMLModel("", *nmlLODTreeNode.mutable_model(), textureExporter);
		Bounds3 modelBounds = combinedModel.calculateBounds(Matrix4::IDENTITY);
		modelTreeBounds.add(modelBounds);
		Bounds3 nodeBounds(globalTransform.inverse() * spatialTreeNode->getNodeBounds().min, globalTransform.inverse() * spatialTreeNode->getNodeBounds().max);
		Vector3 delta = modelBounds.center() - nodeBounds.center();
		nodeBounds.min += delta; nodeBounds.max += delta;
		*nmlLODTreeNode.mutable_bounds() = nodeBounds.getNMLBounds();

		// Save node
		mWriter.saveNMLModelLODTreeNode(nmlLODTreeNode);

		// Release children (not needed anymore) to reduce memory usage
		for (int n = 0; n < SpatialModelTree::Node::CHILDREN; n++)
		{
			spatialTreeNode->setChild(n, 0);
		}

		// Return the index of added node
		return nmlNodeIdx;
	}

	//--------------------------------------------------------------------
	void GeoJsonModelLODTreeBuilder::loadModelData(ModelData &modelData, const Vector3 &wgs84Location)
	{
		ModelInfo modelInfo = mModelMap[modelData.modelId];
		modelData.id = modelInfo.id;
		modelData.folder = modelInfo.folder;
		modelData.height = modelInfo.height;
		modelData.contours.push_back(std::make_pair(modelData.modelId, modelInfo.contour));
		Matrix4 localTransform = internalWgs84Transformation(wgs84Location).inverse();
		for (size_t i = 0; i < modelData.contours.back().second.size(); i++)
		{
			modelData.contours.back().second[i] = localTransform * modelData.contours.back().second[i];
		}
	}

	//--------------------------------------------------------------------
	void GeoJsonModelLODTreeBuilder::concatenateModelData(ModelData &destModelData, const ModelData &srcModelData)
	{
		for (size_t i = 0; i < srcModelData.contours.size(); i++)
		{
			destModelData.contours.push_back(srcModelData.contours[i]);
		}
	}

	//--------------------------------------------------------------------
	void GeoJsonModelLODTreeBuilder::simplifyContour(Contour &contour, const Matrix4 &transform, const Vector3 &boundsSize) const
	{
		while (contour.size() > 4)
		{
			int best_index = -1;
			double best_error = getOptions().maxSimplifyError;
			for (int i = 0; i < (int) contour.size(); i++)
			{
				Vector3 a = transform * contour[(i + 0) % contour.size()];
				Vector3 b = transform * contour[(i + 1) % contour.size()];
				Vector3 c = transform * contour[(i + 2) % contour.size()];
				Vector3 ab = b - a;
				Vector3 ac = c - a;
				Vector3 delta = ab - ab * ab.normalisedCopy().dotProduct(ac.normalisedCopy());
				double error = delta.length() / std::min(boundsSize.x, boundsSize.y);
				if (error < best_error)
				{
					best_index = (i + 1) % contour.size();
					best_error = error;
				}
			}
			if (best_index == -1)
				break;
			contour.erase(contour.begin() + best_index);
		}
	}

	//--------------------------------------------------------------------
	Model GeoJsonModelLODTreeBuilder::createSimplifiedModel(const ModelData &modelData, const Matrix4 &transform, const Vector3 &boundsSize) const
	{
		Model floorModels, sidesModels, roofModels;
		for (size_t i = 0; i < modelData.contours.size(); i++)
		{
			ModelId id = modelData.contours[i].first;
			Contour contour = modelData.contours[i].second;
			simplifyContour(contour, transform, boundsSize);

			Model floorModel = createContourModel(contour, 0, getOptions().floorColor);
			floorModel.setVertexIds(id);
			floorModels.concat(floorModel);

			Model sidesModel = createSidesModel(contour, modelData.height);
			sidesModel.setVertexIds(id);
			sidesModels.concat(sidesModel);

			Model roofModel = createContourModel(contour, modelData.height, getOptions().roofColor);
			roofModel.setVertexIds(id);
			roofModels.concat(roofModel);
		}

		Model models;
		models.concat(floorModels);
		models.concat(sidesModels);
		models.concat(roofModels);
		return models;
	}

	//--------------------------------------------------------------------
	Model GeoJsonModelLODTreeBuilder::createSidesModel(const Contour &contour, double height) const
	{
		static const float TRANSLUCENCY_THRESHOLD = 0.95f;
		
		if (getOptions().wallColor.isTransparent())
			return Model();

		Submesh submesh;
		std::vector<Vector3> posList;
		for (size_t i = 0; i < contour.size(); i++)
		{
			Vector3 p0 = contour[(i + 0) % contour.size()];
			Vector3 p1 = contour[(i + 1) % contour.size()];
			Vector3 n(p0.y - p1.y, p1.x - p0.x, 0);

			posList.clear();
			posList.push_back(Vector3(p0.x, p0.y, 0));
			posList.push_back(Vector3(p0.x, p0.y, height));
			posList.push_back(Vector3(p1.x, p1.y, height));
			posList.push_back(Vector3(p0.x, p0.y, 0));
			posList.push_back(Vector3(p1.x, p1.y, height));
			posList.push_back(Vector3(p1.x, p1.y, 0));
			
			for (int j = 0; j < 6; j++)
			{
				appendPosition(submesh, posList[j]);
				appendNormal(submesh, n);
			}

			if (getOptions().twoSidedWalls)
			{
				for (int j = 0; j < 6; j++)
				{
					appendPosition(submesh, posList[5 - j]);
					appendNormal(submesh, -n);
				}
			}
		}
		submesh.getVertexCounts().push_back(submesh.getPositions().size() / 3);
		submesh.setMaterialId("sides");

		Mesh mesh;
		mesh.getSubmeshList().push_back(submesh);

		Material material;
		material.setType(NMLPackage::Material::LAMBERT);
		material.setTranslucent(getOptions().wallColor.a < TRANSLUCENCY_THRESHOLD);
		material.setCulling(getOptions().twoSidedWalls ? NMLPackage::Material::BACK : NMLPackage::Material::NONE);
		Material::ColorOrTexture colorOrTex;
		colorOrTex.set_type(NMLPackage::ColorOrTexture::COLOR);
		*colorOrTex.mutable_color() = getOptions().wallColor.getNMLColor();
		material.getChannelMap()[Material::DIFFUSE] = colorOrTex;

		MeshInstance meshInstance;
		meshInstance.getMaterialMap()[submesh.getMaterialId()] = material;
		meshInstance.setMeshId("sides");

		Model model;
		model.getMeshInstanceList().push_back(meshInstance);
		model.getMeshMap()[meshInstance.getMeshId()] = mesh;
		return model;
	}

	//--------------------------------------------------------------------
	Model GeoJsonModelLODTreeBuilder::createContourModel(const Contour &contour, double height, const ColorRGBA &color) const
	{
		static const float TRANSLUCENCY_THRESHOLD = 0.95f;
		
		if (color.isTransparent())
			return Model();

		std::vector<float> points;
		std::vector<int> inIndices;
		std::vector<int> outIndices;
		for (size_t i = 0; i < contour.size(); i++)
		{
			points.push_back((float) contour[i].x);
			points.push_back((float) contour[i].y);
			points.push_back((float) contour[i].z);
			inIndices.push_back((int) i);
		}
		Triangulator triangulator(&points[0]);
		triangulator.triangulate(inIndices, outIndices);

		Submesh submesh;
		for (size_t i = 0; i < outIndices.size(); i++)
		{
			Vector3 p = contour[outIndices[i]];
			appendPosition(submesh, Vector3(p.x, p.y, height));
			Vector3 n(0, 0, 1);
			appendNormal(submesh, n);
		}
		submesh.getVertexCounts().push_back(submesh.getPositions().size() / 3);
		submesh.setMaterialId("contour");

		Mesh mesh;
		mesh.getSubmeshList().push_back(submesh);

		Material material;
		material.setType(NMLPackage::Material::LAMBERT);
		material.setTranslucent(color.a < TRANSLUCENCY_THRESHOLD);
		material.setCulling(NMLPackage::Material::NONE);
		Material::ColorOrTexture colorOrTex;
		colorOrTex.set_type(NMLPackage::ColorOrTexture::COLOR);
		*colorOrTex.mutable_color() = color.getNMLColor();
		material.getChannelMap()[Material::DIFFUSE] = colorOrTex;

		MeshInstance meshInstance;
		meshInstance.getMaterialMap()[submesh.getMaterialId()] = material;
		meshInstance.setMeshId("contour");

		Model model;
		model.getMeshInstanceList().push_back(meshInstance);
		model.getMeshMap()[meshInstance.getMeshId()] = mesh;
		return model;
	}

	//--------------------------------------------------------------------
	void GeoJsonModelLODTreeBuilder::removeDuplicateTriangles(Model &model)
	{
		for (Model::MeshMap::iterator meshIt = model.getMeshMap().begin(); meshIt != model.getMeshMap().end(); meshIt++)
		{
			Mesh &mesh = meshIt->second;
			for (Mesh::SubmeshList::iterator submeshIt = mesh.getSubmeshList().begin(); submeshIt != mesh.getSubmeshList().end(); submeshIt++)
			{
				Submesh &submesh = *submeshIt;
				if (submesh.getType() != NMLPackage::Submesh::TRIANGLES)
					continue;

				std::map<float, std::vector<size_t> > triangleMap;
				size_t offset = 0;
				for (size_t i = 0; i < submesh.getVertexCounts().size(); i++)
				{
					size_t count = submesh.getVertexCounts()[i];
					for (size_t j = 0; j < count - 2; )
					{
						int index = offset + j;
						std::vector<size_t> &indices = triangleMap[submesh.getPositions()[index * 3]];
						
						bool found = false;
						for (size_t k = 0; k < indices.size(); k++)
						{
							found = true;
							size_t v0 = index;
							size_t v1 = indices[k];
							for (size_t v = 0; v < 3; v++)
							{
								if (!std::equal(submesh.getPositions().begin() + v0 * 3, submesh.getPositions().begin() + v0 * 3 + 3, submesh.getPositions().begin() + v1 * 3))
									found = false;
								v0 = v0 / 3 * 3 + (v0 + 1) % 3;
								v1 = v1 / 3 * 3 + (v1 + 1) % 3;
							}
							if (found)
								break;
						}
						
						if (found)
						{
							submesh.getPositions().erase(submesh.getPositions().begin() + index * 3, submesh.getPositions().begin() + (index + 3) * 3);
							if (!submesh.getNormals().empty())
								submesh.getNormals().erase(submesh.getNormals().begin() + index * 3, submesh.getNormals().begin() + (index + 3) * 3);
							if (!submesh.getUVs().empty())
								submesh.getUVs().erase(submesh.getUVs().begin() + index * 2, submesh.getUVs().begin() + (index + 3) * 2);
							if (!submesh.getColors().empty())
								submesh.getColors().erase(submesh.getColors().begin() + index * 4, submesh.getColors().begin() + (index + 3) * 4);
							if (!submesh.getVertexIds().empty())
								submesh.getVertexIds().erase(submesh.getVertexIds().begin() + index, submesh.getVertexIds().begin() + (index + 3));
							count -= 3;
						}
						else
						{
							indices.push_back(index);
							j++;
						}
					}
					offset += count;
					submesh.getVertexCounts()[i] = count;
				}
			}
		}
	}

} // namespace POI2Sqlite
