/*
Copyright (c) 2012 Nutiteq

This file is part of POI2Sqlite.
*/

#ifndef __GEOJSON2SQLITE_GEOJSONMODELLODTREEBUILDER_H__
#define __GEOJSON2SQLITE_GEOJSONMODELLODTREEBUILDER_H__

#include "Prerequisites.h"
#include "Processor.h"
#include "SpatialTree.h"
#include "Model.h"

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>


namespace GeoJson2Db
{
	using namespace NMLFramework;

	class GeoJsonModelLODTreeBuilder : public Processor
	{

	public:

		typedef int ModelId;

		class IWriter
		{
		public:
			virtual ~IWriter() { }
			virtual void saveNMLModelLODTreeNode(NMLPackage::ModelLODTreeNode &nmlLODTreeNode) = 0;
		};

		typedef std::vector<Vector3> Contour;

		struct ModelInfo
		{
			std::string id;
			std::string folder;
			double height;
			Contour contour;
			std::map<std::string, std::string> customPropertyMap;

			ModelInfo() : id(), folder(), height(0), contour(), customPropertyMap() { }
		};

		struct Options
		{
			int lightingModel;
			int targetScreenSize;
			int maxSubmeshVertices;
			int maxExtraFlattenVertices;
			int maxMergedColorVertices;
			double minSubmeshSize;
			double initialNodeSize;
			double finalNodeSize;
			double maxSimplifyError;
			bool twoSidedWalls;
			ColorRGBA roofColor;
			ColorRGBA floorColor;
			ColorRGBA wallColor;

			Options() : lightingModel(1), targetScreenSize(256), maxSubmeshVertices(4096),  maxExtraFlattenVertices(256), maxMergedColorVertices(4096), minSubmeshSize(0), initialNodeSize(2000.0), finalNodeSize(20.0), maxSimplifyError(0.0001), twoSidedWalls(false), roofColor(0.6f, 0, 0, 1), floorColor(0, 0, 0, 0), wallColor(1, 1, 1, 1) { }

			void setVariables(const boost::program_options::variables_map &vm)
			{
				lightingModel = vm["lighting-model"].as<int>();
				targetScreenSize = vm["target-screen-size"].as<int>();
				maxSubmeshVertices = vm["max-submesh-vertices"].as<int>();
				maxExtraFlattenVertices = vm["max-extra-flatten-vertices"].as<int>();
				maxMergedColorVertices = vm["max-merged-color-vertices"].as<int>();
				minSubmeshSize = vm["min-submesh-size"].as<double>();
				initialNodeSize = vm["initial-node-size"].as<double>();
				finalNodeSize = vm["final-node-size"].as<double>();
				maxSimplifyError = vm["max-simplify-error"].as<double>();
				twoSidedWalls = vm["two-sided-walls"].as<bool>();
				roofColor = vm["roof-color"].as<ColorRGBA>();
				floorColor = vm["floor-color"].as<ColorRGBA>();
				wallColor = vm["wall-color"].as<ColorRGBA>();
			}

			static boost::program_options::options_description getDescription()
			{
				namespace po = boost::program_options;
				static const Options opt;
				po::options_description desc("LOD tree options");
				desc.add_options()
					("lighting-model", po::value<int>()->default_value(opt.lightingModel), "lighting model (0=none, 1=directional sun)")
					("target-screen-size", po::value<int>()->default_value(opt.targetScreenSize), "target screen size")
					("max-submesh-vertices", po::value<int>()->default_value(opt.maxSubmeshVertices), "maximum submesh vertices")
					("max-extra-flatten-vertices", po::value<int>()->default_value(opt.maxExtraFlattenVertices), "maximum extra vertices flattening may generate per mesh")
					("max-merged-color-vertices", po::value<int>()->default_value(opt.maxMergedColorVertices), "maximum merged color vertices")
					("min-submesh-size", po::value<double>()->default_value(opt.minSubmeshSize), "minimum relative submesh size")
					("initial-node-size", po::value<double>()->default_value(opt.initialNodeSize), "initial LOD tree node size (meters)")
					("final-node-size", po::value<double>()->default_value(opt.finalNodeSize), "final LOD tree node size (meters)")
					("max-simplify-error", po::value<double>()->default_value(opt.maxSimplifyError), "maximum simplification error (relative)")
					("two-sided-walls", po::value<bool>()->default_value(opt.twoSidedWalls), "use two-sided walls")
					("roof-color", po::value<ColorRGBA>()->default_value(opt.roofColor), "roof color (rgba)")
					("floor-color", po::value<ColorRGBA>()->default_value(opt.floorColor), "floor color (rgba)")
					("wall-color", po::value<ColorRGBA>()->default_value(opt.wallColor), "wall color (rgba)")
				;
				return desc;
			}
		};

		GeoJsonModelLODTreeBuilder(IWriter &writer) : mWriter(writer) { }

		/** Clear model references */
		void clear() { mModelMap.clear(); }

		/** Add model reference to model list, return model id */
		ModelId add(const ModelInfo &modelInfo) { ModelId modelId = (ModelId) mModelMap.size() + 1; mModelMap[modelId] = modelInfo; return modelId; }

		/** Generate NML model LOD tree. Also return base location for the tree */
		void buildNMLModelLODTree(NMLPackage::ModelLODTree &nmlLODTree, Vector3 &mapPos, Bounds3 &localBounds);

		/** Options */
		static Options &getOptions() { return sOptions; }

	private:

		struct ModelData
		{
			ModelId modelId; // mModelMap key
			std::string id;
			std::string folder;
			double height;
			std::vector<std::pair<ModelId, Contour> > contours;
		};

		typedef std::map<ModelId, ModelInfo> ModelMap;
		typedef SpatialTree<ModelData, 3> SpatialModelTree;

		IWriter &mWriter;
		ModelMap mModelMap;

		static Options sOptions;

		int createNode(SpatialModelTree::Node *spatialTreeNode, NMLPackage::ModelLODTree &nmlLODTree, Bounds3 &modelTreeBounds, const Vector3 &wgs84Location);
		void loadModelData(ModelData &modelData, const Vector3 &wgs84Location);
		void concatenateModelData(ModelData &destModelData, const ModelData &srcModelData);
		Model createSimplifiedModel(const ModelData &modelData, const Matrix4 &globalTransform, const Vector3 &boundsSize) const;
		Model createContourModel(const Contour &contour, double height, const ColorRGBA &color) const;
		Model createSidesModel(const Contour &contour, double height) const;
		void simplifyContour(Contour &contour, const Matrix4 &transform, const Vector3 &boundsSize) const;
		void removeDuplicateTriangles(Model &model);
		void appendPosition(Submesh &submesh, const Vector3 &pos) const { submesh.getPositions().push_back(pos.x); submesh.getPositions().push_back(pos.y); submesh.getPositions().push_back(pos.z); }
		void appendNormal(Submesh &submesh, const Vector3 &normal) const { submesh.getNormals().push_back(normal.x); submesh.getNormals().push_back(normal.y); submesh.getNormals().push_back(normal.z); }
	};

} // namespace GeoJson2Db

#endif // __GEOJSON2SQLITE_GEOJSONMODELLODTREEBUILDER_H__
