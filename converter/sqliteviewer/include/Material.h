#pragma once

#include "BaseTypes.h"
#include "Texture.h"

#include <list>
#include <map>
#include <string>
#include <cassert>
#include <boost/shared_ptr.hpp>

namespace SqliteViewer
{

	class GLMaterial
	{

	private:

		// Material parameter that can be either constant color or texture
		struct ColorOrTexture
		{
			std::string textureId;
			GLTexturePtr texture;
			float color[4];

			ColorOrTexture() { }

			ColorOrTexture(bool hasColorOrTexture, const NMLPackage::ColorOrTexture &colorOrTexture, const std::map<std::string, GLTexturePtr> &textureMap)
			{
				if (hasColorOrTexture)
				{
					if (colorOrTexture.has_texture_id())
					{
						textureId = colorOrTexture.texture_id();
						std::map<std::string, GLTexturePtr>::const_iterator it = textureMap.find(colorOrTexture.texture_id());
						if (it != textureMap.end())
							texture = it->second;
					}
					if (colorOrTexture.has_color())
					{
						NMLPackage::ColorRGBA c = colorOrTexture.color();
						color[0] = c.r(); color[1] = c.g(); color[2] = c.b(); color[3] = c.a();
					}
				}
			}
		};

		// Material attributes
		NMLPackage::Material::Culling mCulling;
		ColorOrTexture mDiffuse;
		// TODO: other attributes

	public:

		GLMaterial() : mCulling(NMLPackage::Material::NONE) { }

		void create(GL10 gl, NMLPackage::Material material, const std::map<std::string, GLTexturePtr> &textureMap)
		{
			mCulling = material.culling();
			mDiffuse = ColorOrTexture(material.has_diffuse(), material.diffuse(), textureMap);
			// TODO: use other attributes
		}

		void replaceTexture(const std::string &textureId, const GLTexturePtr &glTexture)
		{
			if (mDiffuse.textureId == textureId)
				mDiffuse.texture = glTexture;
			// TODO: other attributes
		}

		void bind(GL10 gl)
		{
			if (mCulling == NMLPackage::Material::NONE) {
				glDisable(GL_CULL_FACE);
			} else {
				glEnable(GL_CULL_FACE);
				glCullFace(mCulling == NMLPackage::Material::FRONT ? GL_FRONT : GL_BACK);
			}
			if (mDiffuse.texture && !mDiffuse.texture->isEmpty()) {
				glColor4f(1, 1, 1, 1);
				glEnable(GL_TEXTURE_2D);
				mDiffuse.texture->bind(gl);
			}
			else {
				glColor4f(mDiffuse.color[0], mDiffuse.color[1], mDiffuse.color[2], mDiffuse.color[3]);
				glDisable(GL_TEXTURE_2D);
			}
			// TODO: use texture combiners for all material attributes
		}

		void unbind(GL10 gl)
		{
			if (mCulling != NMLPackage::Material::NONE) {
				glDisable(GL_CULL_FACE);
			}
			if (mDiffuse.texture && !mDiffuse.texture->isEmpty()) {
				mDiffuse.texture->unbind(gl);
			}
		}
	};

	typedef boost::shared_ptr<GLMaterial> GLMaterialPtr;

}
