#pragma once

#include "BaseTypes.h"

#include <vector>
#include <cassert>
#include <boost/shared_ptr.hpp>
#include <GL/gl.h>

namespace SqliteViewer
{

	class GLSubmesh
	{

	private:

		typedef std::vector<unsigned char> ByteBuffer;
		typedef std::vector<float> FloatBuffer;

		GLuint mGLType;
		std::vector<int> mVertexCounts;
		std::string mMaterialId;

		FloatBuffer mPositionBuffer;
		FloatBuffer mNormalBuffer;
		FloatBuffer mUVBuffer;
		ByteBuffer mColorBuffer;
		ByteBuffer mVertexIdBuffer;

		// Convert ProtoBuf big-endian floating point buffer to native direct-allocated buffer that can be used as a GL vertex array
		static FloatBuffer convertToFloatBuffer(const std::string &str)
		{
			if (str.empty())
				return FloatBuffer();
			FloatBuffer buf(reinterpret_cast<const float *>(str.data()), reinterpret_cast<const float *>(str.data()) + str.size() / sizeof(float));
			return buf;
		}

		static ByteBuffer convertToByteBuffer(const std::string &str)
		{
			if (str.empty())
				return ByteBuffer();
			ByteBuffer buf(reinterpret_cast<const unsigned char *>(str.data()), reinterpret_cast<const unsigned char *>(str.data()) + str.size() / sizeof(unsigned char));
			return buf;
		}

		static GLint convertType(int type)
		{
			GLint glType = -1;
			switch (type)
			{
			case NMLPackage::Submesh::POINTS:
				glType = GL_POINTS;
				break;
			case NMLPackage::Submesh::LINES:
				glType = GL_LINES;
				break;
			case NMLPackage::Submesh::LINE_STRIPS:
				glType = GL_LINE_STRIP;
				break;
			case NMLPackage::Submesh::TRIANGLES:
				glType = GL_TRIANGLES;
				break;
			case NMLPackage::Submesh::TRIANGLE_STRIPS:
				glType = GL_TRIANGLE_STRIP;
				break;
			case NMLPackage::Submesh::TRIANGLE_FANS:
				glType = GL_TRIANGLE_FAN;
				break;
			default:
				assert(false);
			}
			return glType;
		}

		static void encodeId(int id, unsigned char buf[])
		{
			buf[0] = (unsigned char) ((id >> 16) & 255);
			buf[1] = (unsigned char) ((id >>  8) & 255);
			buf[2] = (unsigned char) ((id >>  0) & 255);
		}

		static void decodeId(const unsigned char buf[], int &id)
		{
			id  = (int) buf[0] << 16;
			id |= (int) buf[1] <<  8;
			id |= (int) buf[2] <<  0;
		}

	public:

		GLSubmesh() : mGLType(-1) { }

		void create(GL10 gl, const NMLPackage::Submesh &submesh)
		{
			// Translate submesh type
			mGLType = convertType(submesh.type());

			// Copy basic attributes
			mVertexCounts.clear();
			for (int i = 0; i < submesh.vertex_counts_size(); i++)
			{
				mVertexCounts.push_back(submesh.vertex_counts(i));
			}
			mMaterialId = submesh.material_id();

			// Create vertex buffers
			mPositionBuffer = convertToFloatBuffer(submesh.positions());
			mNormalBuffer = convertToFloatBuffer(submesh.normals());
			mUVBuffer = convertToFloatBuffer(submesh.uvs());
			mColorBuffer = convertToByteBuffer(submesh.colors());

			// Generate vertex id buffer
			mVertexIdBuffer.clear();
			for (int i = 0; i < submesh.vertex_ids_size(); i++)
			{
				int count = (int) (submesh.vertex_ids(i) >> 32);
				int id = (int) (submesh.vertex_ids(i) & (unsigned int) -1);
				unsigned char color[3]; encodeId(id, color);
				for (int j = 0; j < count; j++)
				{
					mVertexIdBuffer.push_back(color[0]);
					mVertexIdBuffer.push_back(color[1]);
					mVertexIdBuffer.push_back(color[2]);
				}
			}
		}

		void createFromOpList(GL10 gl, const NMLPackage::Mesh &mesh, const NMLPackage::SubmeshOpList &submeshOpList)
		{
			mGLType = convertType(submeshOpList.type());
			mMaterialId = submeshOpList.material_id();
			
			mVertexCounts.assign(1, 0);
			mPositionBuffer.clear();
			mNormalBuffer.clear();
			mColorBuffer.clear();
			mVertexIdBuffer.clear();
			mUVBuffer.clear();
			for (int i = 0; i < submeshOpList.submesh_ops_size(); i++)
			{
				const NMLPackage::SubmeshOp &submeshOp = submeshOpList.submesh_ops(i);
				const NMLPackage::Submesh &submesh = mesh.submeshes(submeshOp.submesh_idx());
				// TODO: unoptimal, should extract directly required slices
				GLSubmesh tempGLSubmesh;
				tempGLSubmesh.create(gl, submesh);

				int start = submeshOp.offset(), end = submeshOp.offset() + submeshOp.count();
				mVertexCounts[0] += submeshOp.count();
				mPositionBuffer.insert(mPositionBuffer.end(), tempGLSubmesh.mPositionBuffer.begin() + start * 3, tempGLSubmesh.mPositionBuffer.begin() + end * 3);
				if (!tempGLSubmesh.mNormalBuffer.empty())
					mNormalBuffer.insert(mNormalBuffer.end(), tempGLSubmesh.mNormalBuffer.begin() + start * 3, tempGLSubmesh.mNormalBuffer.begin() + end * 3);
				if (!tempGLSubmesh.mColorBuffer.empty())
					mColorBuffer.insert(mColorBuffer.end(), tempGLSubmesh.mColorBuffer.begin() + start * 4, tempGLSubmesh.mColorBuffer.begin() + end * 4);
				if (!tempGLSubmesh.mVertexIdBuffer.empty())
					mVertexIdBuffer.insert(mVertexIdBuffer.end(), tempGLSubmesh.mVertexIdBuffer.begin() + start * 3, tempGLSubmesh.mVertexIdBuffer.begin() + end * 3);
				if (!tempGLSubmesh.mUVBuffer.empty())
				{
					for (int idx = start; idx < end; idx++)
					{
						float u = tempGLSubmesh.mUVBuffer[idx * 2 + 0] * submeshOp.tex_u_scale() + submeshOp.tex_u_trans();
						float v = tempGLSubmesh.mUVBuffer[idx * 2 + 1] * submeshOp.tex_v_scale() + submeshOp.tex_v_trans();
						mUVBuffer.push_back(u);
						mUVBuffer.push_back(v);
					}
				}
			}
		}

		void draw(GL10 gl, bool bindVertexIds) const
		{
			if (mVertexCounts.empty())
				return;

			// Enable vertex buffers
			if (!mPositionBuffer.empty()) {
				glEnableClientState(GL_VERTEX_ARRAY);
				glVertexPointer(3, GL_FLOAT, 0, &mPositionBuffer[0]);
			}
			if (!mNormalBuffer.empty()) {
				glEnableClientState(GL_NORMAL_ARRAY);
				glNormalPointer(GL_FLOAT, 0, &mNormalBuffer[0]);
			}
			if (!mUVBuffer.empty()) {
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				glTexCoordPointer(2, GL_FLOAT, 0, &mUVBuffer[0]);
			}
			if (!bindVertexIds && !mColorBuffer.empty()) {
				glEnableClientState(GL_COLOR_ARRAY);
				glColorPointer(4, GL_UNSIGNED_BYTE, 0, &mColorBuffer[0]);
			}
			if (bindVertexIds && !mVertexIdBuffer.empty()) {
				glEnableClientState(GL_COLOR_ARRAY);
				glColorPointer(3, GL_UNSIGNED_BYTE, 0, &mVertexIdBuffer[0]);
			}

			// Draw primitives
			int idx = 0;
			for (size_t i = 0; i < mVertexCounts.size(); i++)
			{
				int count = mVertexCounts[i];
				glDrawArrays(mGLType, idx, count);
				idx += count;
			}

			// Disable vertex buffers
			if (!bindVertexIds && !mColorBuffer.empty()) {
				glDisableClientState(GL_COLOR_ARRAY);
			}
			if (bindVertexIds && !mVertexIdBuffer.empty()) {
				glDisableClientState(GL_COLOR_ARRAY);
			}
			if (!mUVBuffer.empty()) {
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			}
			if (!mNormalBuffer.empty()) {
				glDisableClientState(GL_NORMAL_ARRAY);
			}
			if (!mPositionBuffer.empty()) {
				glDisableClientState(GL_VERTEX_ARRAY);
			}
		}

		std::string getMaterialId() const
		{
			return mMaterialId;
		}

		int getDrawCallCount() const
		{
			return (int) mVertexCounts.size();
		}

		int getTotalGeometrySize() const
		{
			int size = 0;
			size += mPositionBuffer.size() * sizeof(FloatBuffer::value_type);
			size += mNormalBuffer.size() * sizeof(FloatBuffer::value_type);
			size += mUVBuffer.size() * sizeof(FloatBuffer::value_type);
			size += mColorBuffer.size() * sizeof(ByteBuffer::value_type);
			size += mVertexIdBuffer.size() * sizeof(ByteBuffer::value_type);
			return size;
		}
	};

	typedef boost::shared_ptr<GLSubmesh> GLSubmeshPtr;

}
