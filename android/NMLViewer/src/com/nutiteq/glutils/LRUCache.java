package com.nutiteq.glutils;

import java.util.HashMap;

// Class providing fixed-size LRU-replacement cache
public class LRUCache<K, V> {

  private class Node {
    public K key;
    public Node prev;
    public Node next;
  }

  private class Record {
    public V value;
    public int size;
    public Node node;
  }

  private final int mCapacity;
  private int mResident;
  private Node mFirstNode = null;
  private Node mLastNode = null;
  private HashMap<K, Record> mRecordMap = new HashMap<K, Record>();

  private void access(Node node) {
    if (node == mLastNode)
      return;
    if (node.prev != null)
      node.prev.next = node.next;
    if (node.next != null)
      node.next.prev = node.prev;
    if (mFirstNode == node)
      mFirstNode = node.next;
    node.next = null;
    node.prev = mLastNode;
    mLastNode.next = node;
    mLastNode = node;
  }

  private void evict(boolean all, Disposer<K, V> disposer) {
    Node node = mFirstNode;
    while (node != null) {
      if (!all && mResident <= mCapacity)
        break;

      Record record = mRecordMap.get(node.key);
      if (!disposer.dispose(node.key, record.value)) {
        node = node.next;
        continue;
      }

      if (node.prev != null)
        node.prev.next = node.next;
      if (node.next != null)
        node.next.prev = node.prev;
      if (mFirstNode == node)
        mFirstNode = node.next;
      if (mLastNode == node)
        mLastNode = node.prev;
      mRecordMap.remove(node.key);
      mResident -= record.size;
      node = node.next;
    }
  }

  // Disposer interface - we will allow customization of disposing and disposal cancellation 
  public interface Disposer<K, V> {
    boolean dispose(K key, V value);
  }

  // Create cache with given maximum capacity
  public LRUCache(int capacity) {
    mCapacity = capacity;
    mResident = 0;
  }

  // Test if key/value exists in cache. Item ordering is not changed by calling this method.
  public boolean exists(K key) {
    return mRecordMap.get(key) != null;
  }

  // Try to get value with given key. If key exists, then value is the last to be evicted.
  public V get(K key) {
    Record record = mRecordMap.get(key);
    if (record == null)
      return null;

    access(record.node);
    return record.value;
  }

  // Store value given key, value and estimated value size.
  // Note: at this point no values are automatically evicted, so container can grow bigger than maximum capacity.
  public void put(K key, V value, int size) {
    Record record = mRecordMap.get(key);
    if (record == null) {
      Node node = new Node();
      node.key = key;
      node.next = null;
      node.prev = mLastNode;
      if (mLastNode != null)
        mLastNode.next = node;
      if (mFirstNode == null)
        mFirstNode = node;
      mLastNode = node;

      record = new Record();
      record.value = value;
      record.size = size;
      record.node = mLastNode;
      mRecordMap.put(key, record);
      mResident += size;
    }
    else {
      access(record.node);
      mResident -= record.size;
      record.value = value;
      record.size = size;
      mResident += size;
    }
  }

  // Evict values that are least recently used until capacity constraint is satisfied.
  // Given disposer can do customized resource disposing and can decide whether a given value should be disposed or not.
  public void evict(Disposer<K, V> disposer) {
    evict(false, disposer);
  }

  // Evict all values from cache.
  // Given disposer can do customized resource disposing and can decide whether a given value should be disposed or not.
  public void evictAll(Disposer<K, V> disposer) {
    evict(true, disposer);
  }

  // Get total resident cache size
  public int resident() {
    return mResident;
  }

  // Get cache capacity
  public int capacity() {
    return mCapacity;
  }

}
