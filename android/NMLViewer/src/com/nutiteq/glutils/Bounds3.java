package com.nutiteq.glutils;

public class Bounds3 {
  private float[] mMin;
  private float[] mMax;

  public Bounds3() {
    mMin = new float[] {  Float.MAX_VALUE,  Float.MAX_VALUE,  Float.MAX_VALUE };
    mMax = new float[] { -Float.MAX_VALUE, -Float.MAX_VALUE, -Float.MAX_VALUE };
  }

  public Bounds3(float[] min, float[] max) {
    mMin = min.clone();
    mMax = max.clone();
  }

  public boolean isEmpty() {
    for (int i = 0; i < 3; i++) {
      if (mMin[i] >= mMax[i])
        return true;
    }
    return false;
  }

  public float[] getMin() {
    return mMin;
  }

  public float[] getMax() {
    return mMax;
  }

  public float[] getCenter() {
    return new float[] { (mMin[0] + mMax[0]) * 0.5f, (mMin[1] + mMax[1]) * 0.5f, (mMin[2] + mMax[2]) * 0.5f };
  }
}
