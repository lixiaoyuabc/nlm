package com.nutiteq.glutils;

public class Frustum3 {
  private float[][] mPlanes;

  private float getPlaneDistance(int p, float[] point) {
    float d = mPlanes[p][3];
    for (int i = 0; i < 3; i++) {
      d += mPlanes[p][i] * point[i];
    }
    return d;
  }

  public Frustum3(float[][] planes) {
    mPlanes = new float[6][];
    for (int i = 0; i < 6; i++) {
      mPlanes[i] = planes[i].clone();
    }
  }

  public boolean isInside(Bounds3 b) {
    float[] point = new float[3];
    float[] min = b.getMin(), max = b.getMax();
    for (int p = 0; p < 6; p++) {
      int i;
      for (i = 0; i < 8; i++) {
        point[0] = (i & 1) != 0 ? min[0] : max[0];
        point[1] = (i & 2) != 0 ? min[1] : max[1];
        point[2] = (i & 4) != 0 ? min[2] : max[2];
        float d = getPlaneDistance(p, point);
        if (d > 0)
          break;
      }
      if (i == 8)
        return false;
    }
    return true;
  }

  public float[] getPlane(int p) {
    return mPlanes[p];
  }

  public static Frustum3 fromGLProjectionMatrix(Matrix4 proj) {
    float[][] planes = new float[6][];
    planes[5] = new float[] { proj.get(3, 0) + proj.get(0, 0), proj.get(3, 1) + proj.get(0, 1), proj.get(3, 2) + proj.get(0, 2), proj.get(3, 3) + proj.get(0, 3) };
    planes[4] = new float[] { proj.get(3, 0) - proj.get(0, 0), proj.get(3, 1) - proj.get(0, 1), proj.get(3, 2) - proj.get(0, 2), proj.get(3, 3) - proj.get(0, 3) };
    planes[3] = new float[] { proj.get(3, 0) + proj.get(1, 0), proj.get(3, 1) + proj.get(1, 1), proj.get(3, 2) + proj.get(1, 2), proj.get(3, 3) + proj.get(1, 3) };
    planes[2] = new float[] { proj.get(3, 0) - proj.get(1, 0), proj.get(3, 1) - proj.get(1, 1), proj.get(3, 2) - proj.get(1, 2), proj.get(3, 3) - proj.get(1, 3) };
    planes[1] = new float[] { proj.get(3, 0) + proj.get(2, 0), proj.get(3, 1) + proj.get(2, 1), proj.get(3, 2) + proj.get(2, 2), proj.get(3, 3) + proj.get(2, 3) };
    planes[0] = new float[] { proj.get(3, 0) - proj.get(2, 0), proj.get(3, 1) - proj.get(2, 1), proj.get(3, 2) - proj.get(2, 2), proj.get(3, 3) - proj.get(2, 3) };
    return new Frustum3(planes);
  }
}
