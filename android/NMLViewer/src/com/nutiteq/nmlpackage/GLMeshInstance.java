package com.nutiteq.nmlpackage;

import java.util.HashMap;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import com.nutiteq.nmlpackage.NMLPackage.Matrix4;

public class GLMeshInstance {
  private String mMeshId; 
  private GLMesh mMesh = null;
  private float[] mTransform; // null (identity transform) or 4x4 matrix, 16 elements
  private Map<String, GLMaterial> mMaterialMap = new HashMap<String, GLMaterial>();

  public enum DrawMode {
    DRAW_NORMAL, DRAW_VERTEX_IDS, DRAW_CONST
  };

  public void create(GL10 gl, NMLPackage.MeshInstance meshInstance,
      Map<String, GLMesh> meshMap, Map<String, GLTexture> textureMap) {
    mMeshId = meshInstance.getMeshId();

    // Get mesh
    mMesh = meshMap.get(meshInstance.getMeshId());

    // Get transformation matrix
    mTransform = null;
    if (meshInstance.hasTransform()) {
      Matrix4 transform = meshInstance.getTransform();
      mTransform = new float[16];

      mTransform[0] = transform.getM00();
      mTransform[1] = transform.getM10();
      mTransform[2] = transform.getM20();
      mTransform[3] = transform.getM30();

      mTransform[4] = transform.getM01();
      mTransform[5] = transform.getM11();
      mTransform[6] = transform.getM21();
      mTransform[7] = transform.getM31();

      mTransform[8] = transform.getM02();
      mTransform[9] = transform.getM12();
      mTransform[10] = transform.getM22();
      mTransform[11] = transform.getM32();

      mTransform[12] = transform.getM03();
      mTransform[13] = transform.getM13();
      mTransform[14] = transform.getM23();
      mTransform[15] = transform.getM33();
    }

    // Create material map
    mMaterialMap.clear();
    for (NMLPackage.Material material : meshInstance.getMaterialsList()) {
      GLMaterial glMaterial = new GLMaterial();
      glMaterial.create(gl, material, textureMap);
      mMaterialMap.put(material.getId(), glMaterial);
    }
  }
  
  public void replaceMesh(String meshId, GLMesh glMesh) {
    if (meshId.equals(mMeshId)) {
      mMesh = glMesh;
    }
  }

  public void replaceTexture(String textureId, GLTexture glTexture) {
    for (GLMaterial material : mMaterialMap.values()) {
      material.replaceTexture(textureId, glTexture);
    }
  }

  public void draw(GL10 gl, DrawMode mode) {
    if (mMesh == null)
      return;
    if (mTransform != null) {
      gl.glMatrixMode(GL10.GL_MODELVIEW);
      gl.glPushMatrix();
      gl.glMultMatrixf(mTransform, 0);
    }
    GLSubmesh[] submeshes = mMesh.getSubmeshes();
    if (submeshes != null) {
      for (GLSubmesh submesh : submeshes) {
        GLMaterial material = mMaterialMap.get(submesh.getMaterialId());
        if (material != null) {
          if (mode == DrawMode.DRAW_NORMAL)
            material.bind(gl);
          submesh.draw(gl, mode == DrawMode.DRAW_VERTEX_IDS);
          if (mode == DrawMode.DRAW_NORMAL)
            material.unbind(gl);
        }
      }
    }
    if (mTransform != null) {
      gl.glPopMatrix();
    }
  }
}
