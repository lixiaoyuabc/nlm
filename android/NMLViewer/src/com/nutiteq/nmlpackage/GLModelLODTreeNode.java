package com.nutiteq.nmlpackage;

import java.util.List;

import com.nutiteq.glutils.Bounds3;
import com.nutiteq.nmlpackage.NMLLoader.MeshBinding;
import com.nutiteq.nmlpackage.NMLLoader.TextureBinding;

public class GLModelLODTreeNode {
  
  private int mModelId = -1;
  private int mLevel = -1;
  private Bounds3 mNodeBounds;
  private Bounds3 mModelBounds;
  private MeshBinding[] mMeshBindings;
  private TextureBinding[] mTextureBindings;
  private int[] mChildren;
  private NMLPackage.Model mModel;

  private float[] getVector(NMLPackage.Vector3 v) {
    return new float[] { v.getX(), v.getY(), v.getZ() };
  }

  private Bounds3 getBounds(NMLPackage.Bounds3 b) {
    return new Bounds3(getVector(b.getMin()), getVector(b.getMax()));
  }

  public GLModelLODTreeNode() {
  }

  public void create(NMLPackage.ModelLODTreeNode node, List<MeshBinding> meshBindings, List<TextureBinding> textureBindings, int level) {
    mModelId = node.getId();
    mModel = node.getModel();
    mLevel = level;
    mNodeBounds = getBounds(node.getBounds());
    mModelBounds = getBounds(node.getModel().getBounds());
    mMeshBindings = null;
    if (meshBindings != null) {
      mMeshBindings = new MeshBinding[meshBindings.size()];
      meshBindings.toArray(mMeshBindings);
    }
    mTextureBindings = null;
    if (textureBindings != null) {
      mTextureBindings = new TextureBinding[textureBindings.size()];
      textureBindings.toArray(mTextureBindings);
    }
    mChildren = new int[node.getChildrenIdsCount()];
    for (int i = 0; i < node.getChildrenIdsCount(); i++) {
      mChildren[i] = node.getChildrenIds(i);
    }
  }
  
  public int getModelId() {
    return mModelId;
  }
  
  public NMLPackage.Model getModel() {
    return mModel;
  }

  public int getLevel() {
    return mLevel;
  }

  public Bounds3 getNodeBounds() {
    return mNodeBounds;
  }

  public Bounds3 getModelBounds() {
    return mModelBounds;
  }

  public MeshBinding[] getMeshBindings() {
    return mMeshBindings;
  }

  public TextureBinding[] getTextureBindings() {
    return mTextureBindings;
  }

  public boolean isLeaf() {
    return mChildren == null || mChildren.length == 0;
  }

  public int getChildCount() {
    return mChildren != null ? mChildren.length : 0;
  }

  public int getChild(int index) {
    if (mChildren == null)
      return -1;
    if (index < 0 || index >= mChildren.length)
      return -1;
    return mChildren[index];
  }
}
