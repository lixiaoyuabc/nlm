package com.nutiteq.nmlpackage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

/**
 * Base class for asynchronous NML texture and mesh loader
 */
public abstract class NMLLoader implements Runnable {

  public static class MeshBinding {
    public final String localId;
    public final long meshId;
    
    public MeshBinding(String localId, long meshId) {
      this.localId = localId;
      this.meshId = meshId;
    }
  }
  
  public static class TextureBinding {
    public final String localId;
    public final long textureId;
    public final int level;
    
    public TextureBinding(String localId, long textureId, int level) {
      this.localId = localId;
      this.textureId = textureId;
      this.level = level;
    }
  }
  
  private static class Task {
    enum Type { LOAD_MESH, LOAD_TEXTURE, STOP };
    Type type;
    long meshId;
    long textureId;
    int textureLevel;
    boolean mipChain;
    
    @Override
    public int hashCode() {
      switch (type) {
      case LOAD_MESH:
        return (int) meshId * 2 + 1;
      case LOAD_TEXTURE:
        return (int) textureId * 2 + 2;
      default:
        return 0;
      }
    }
    
    @Override
    public boolean equals(Object obj) {
      if (obj == null || !(obj instanceof Task))
        return false;
      Task other = (Task) obj;
      if (other.type != type)
        return false;
      switch (type) {
      case LOAD_MESH:
    	  return meshId == other.meshId;
      case LOAD_TEXTURE:
    	  return textureId == other.textureId && textureLevel == other.textureLevel; // ignore mipchain deliberately
      default:
    	  return true;
      }
    }
  }

  private LinkedList<Task> mTaskQueue = new LinkedList<Task>();
  private HashSet<Task> mTaskSet = new HashSet<Task>();
  private volatile Map<Long, NMLPackage.Mesh> mMeshMap = new HashMap<Long, NMLPackage.Mesh>();
  private volatile Map<Long, NMLPackage.Texture> mTextureMap = new HashMap<Long, NMLPackage.Texture>();

  private static long encodeTextureIdLevel(long id, int level) {
    return (id << 4) + level;
  }

  protected abstract NMLPackage.Mesh loadMesh(long id);
  protected abstract NMLPackage.Texture loadTexture(long id, int level, boolean mipChain);

  public NMLLoader() {
  }
  
  public boolean isEmpty() {
    synchronized (mTaskQueue) {
      return mTaskQueue.isEmpty() && mMeshMap.isEmpty() && mTextureMap.isEmpty();
    }
  }

  public void requestMesh(long id) {
    Task task = new Task();
    task.type = Task.Type.LOAD_MESH;
    task.meshId = id;
    synchronized (mTaskQueue) {
      if (!mTaskSet.contains(task)) {
        mTaskSet.add(task);
        mTaskQueue.add(task);
        mTaskQueue.notify();
      }
    }
  }

  public void requestTexture(long id, int level, boolean mipChain) {
    Task task = new Task();
    task.type = Task.Type.LOAD_TEXTURE;
    task.textureId = id;
    task.textureLevel = level;
    task.mipChain = mipChain;
    synchronized (mTaskQueue) {
      if (!mTaskSet.contains(task)) {
        mTaskSet.add(task);
        mTaskQueue.add(task);
        mTaskQueue.notify();
      }
    }
  }

  public NMLPackage.Mesh getMesh(long id) {
    return mMeshMap.get(id);
  }

  public NMLPackage.Texture getTexture(long id, int level) {
    return mTextureMap.get(encodeTextureIdLevel(id, level));
  }

  public void removeMesh(long id) {
    Map<Long, NMLPackage.Mesh> meshMap = new HashMap<Long, NMLPackage.Mesh>(mMeshMap); 
    meshMap.remove(id);
    mMeshMap = meshMap;
  }

  public void removeTexture(long id, int level) {
    Map<Long, NMLPackage.Texture> textureMap = new HashMap<Long, NMLPackage.Texture>(mTextureMap);
    textureMap.remove(encodeTextureIdLevel(id, level));
    mTextureMap = textureMap;
  }

  public void flush() {
    mMeshMap = new HashMap<Long, NMLPackage.Mesh>();
    mTextureMap = new HashMap<Long, NMLPackage.Texture>();
  }

  public void run() {
    for (boolean stop = false; !stop; ) {
      Task task;
      synchronized (mTaskQueue) {
        if (mTaskQueue.isEmpty()) {
          try { mTaskQueue.wait(); } catch (InterruptedException e) { }
          continue;
        }
        task = mTaskQueue.getFirst();
      }
      switch (task.type) {
      case STOP:
        stop = true;
        break;
      case LOAD_MESH:
        Map<Long, NMLPackage.Mesh> meshMap = new HashMap<Long, NMLPackage.Mesh>(mMeshMap); 
        meshMap.put(task.meshId, loadMesh(task.meshId));
        mMeshMap = meshMap;
        break;
      case LOAD_TEXTURE:
        Map<Long, NMLPackage.Texture> textureMap = new HashMap<Long, NMLPackage.Texture>(mTextureMap);
        textureMap.put(encodeTextureIdLevel(task.textureId, task.textureLevel), loadTexture(task.textureId, task.textureLevel, task.mipChain));
        mTextureMap = textureMap;
        break;
      }
      synchronized (mTaskQueue) {
        if (mTaskQueue.isEmpty())
          continue;
        if (mTaskQueue.get(0) == task) {
          mTaskQueue.removeFirst();
          mTaskSet.remove(task);
        }
      }
    }
  }

  public void stop() {
    Task task = new Task();
    task.type = Task.Type.STOP;
    synchronized (mTaskQueue) {
      mTaskSet.clear();
      mTaskSet.add(task);
      mTaskQueue.clear();
      mTaskQueue.add(task);
      mTaskQueue.notify();
    }
  }

}

