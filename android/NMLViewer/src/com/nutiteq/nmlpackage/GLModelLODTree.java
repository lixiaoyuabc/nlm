package com.nutiteq.nmlpackage;

import java.util.List;
import java.util.Map;

import com.nutiteq.nmlpackage.NMLLoader.MeshBinding;
import com.nutiteq.nmlpackage.NMLLoader.TextureBinding;

public class GLModelLODTree {
  private GLModelLODTreeNode[] mNodes;
  private Map<Integer, List<MeshBinding>> mMeshBindingMap;
  private Map<Integer, List<TextureBinding>> mTextureBindingMap;

  private void createNode(NMLPackage.ModelLODTree lodTree, int index, int level) {
    if (index == -1)
      return;
    GLModelLODTreeNode node = new GLModelLODTreeNode();
    node.create(lodTree.getNodes(index), mMeshBindingMap.get(index), mTextureBindingMap.get(index), level);
    for (int i = 0; i < node.getChildCount(); i++) {
      createNode(lodTree, node.getChild(i), level + 1);
    }
    mNodes[index] = node;
  }

  public GLModelLODTree() {
  }

  public void create(NMLPackage.ModelLODTree lodTree, Map<Integer, List<MeshBinding>> meshBindingMap, Map<Integer, List<TextureBinding>> textureBindingMap) {
    mNodes = new GLModelLODTreeNode[lodTree.getNodesCount()];
    mMeshBindingMap = meshBindingMap;
    mTextureBindingMap = textureBindingMap;
    if (lodTree.getNodesCount() > 0)
      createNode(lodTree, 0, 0);
  }

  public GLModelLODTreeNode getNode(int index) {
    if (mNodes == null)
      return null;
    if (index < 0 || index >= mNodes.length)
      return null;
    return mNodes[index];
  }
}
